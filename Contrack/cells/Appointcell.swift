//
//  Appointcell.swift
//  Contrack
//
//  Created by Vikram Singh on 15/10/20.
//  Copyright © 2020 Vikram Singh. All rights reserved.
//

import UIKit

class Appointcell: UITableViewCell {

    @IBOutlet weak var txt_status: UILabel!
    
    @IBOutlet weak var txt_technicial: UILabel!
    @IBOutlet weak var txt_schedule: UILabel!
    @IBOutlet weak var txt_address: UILabel!
    @IBOutlet weak var txt_name: UILabel!
    @IBOutlet weak var img_appoint: UIImageView!
    
    @IBOutlet weak var lbl_norecord: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
