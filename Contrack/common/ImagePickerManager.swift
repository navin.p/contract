//
//  ImagePickerManager.swift
//  EPC
//
//  Created by Vkey on 23/10/19.
//  Copyright © 2019 Ascent Cyber Solutions. All rights reserved.
//

import Foundation
import UIKit

class ImagePickerManager: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var picker = UIImagePickerController()
    var alert = UIAlertController(title: Localization("Contract_Choose"), message: nil, preferredStyle: .actionSheet)
    var viewController: UIViewController?
    var pickImageCallback: ((UIImage) -> Void)?

    override init() {
        super.init()
    }

    func pickImage(_ viewController: UIViewController, _ callback: @escaping ((UIImage) -> Void)) {
        pickImageCallback = callback
        self.viewController = viewController

        let cameraAction = UIAlertAction(title: Localization("Contract_Camera"), style: .default) {
            _ in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: Localization("Contract_Gallary"), style: .default) {
            _ in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title:  Localization("Contract_Cancel"), style: .cancel) {
            _ in
        }

        // Add the actions
        picker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        alert.popoverPresentationController?.sourceView = self.viewController!.view
        viewController.present(alert, animated: true, completion: nil)
    }

    func openCamera() {
        alert.dismiss(animated: true, completion: nil)
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.sourceType = .camera
            viewController!.present(picker, animated: true, completion: nil)
        } else {
            let alertWarning = UIAlertView(title: "Warning", message: "You don't have camera", delegate: nil, cancelButtonTitle: "OK", otherButtonTitles: "")
            alertWarning.show()
        }
    }

    func openGallery() {
        alert.dismiss(animated: true, completion: nil)
        picker.sourceType = .photoLibrary
        viewController!.present(picker, animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
          
            
            pickImageCallback?(pickedImage)
        }
    }

 
    
    @objc func imagePickerController(_: UIImagePickerController, pickedImage _: UIImage?) {}
 
    func textToImage(drawText text: String, inImage image: UIImage, atPoint point: CGPoint) -> UIImage {
        


                let outerView = UIView(frame: CGRect(x: 0, y: 0, width: image.size.width / 2, height: image.size.height / 2))
                let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: outerView.frame.width, height: outerView.frame.height))
                imgView.image = image
                outerView.addSubview(imgView)

                let lbl = UILabel(frame: CGRect(x: 5, y: 5, width: outerView.frame.width, height: 200))
                lbl.font = UIFont(name: "HelveticaNeue-Bold", size: 70)
                lbl.text = text
                lbl.textAlignment = .left
                lbl.textColor = UIColor.white

                outerView.addSubview(lbl)

                let renderer = UIGraphicsImageRenderer(size: outerView.bounds.size)
                let convertedImage = renderer.image { ctx in
                    outerView.drawHierarchy(in: outerView.bounds, afterScreenUpdates: true)

                }
                return convertedImage


    }

}

