import Alamofire
import NVActivityIndicatorView
import RappleProgressHUD
import SwiftMessages
import SwiftyJSON
import UIKit
import DatePickerDialog
import CoreLocation

protocol APIDelegate {
    func apiResponse(response: JSON, requestCode: Int)
}


// created by vikram singh
@available(iOS 13.0, *)
class Common: UIViewController {
    
    
    var strLat = "" , strlong = ""
    var locationManager = CLLocationManager()

    
//    Testing :
let BASE_URL  =  "http://testing.contrackmanagementgroup.com:8083/mobile/"
//
//    Production:
  //  let BASE_URL  = "http://contrackmanagementgroup.com:8082/mobile/"
    
    var appThemeColor = "18414B"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    public static func hexStringToUIColor(hex: String) -> UIColor {
        var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }
        
        if cString.count != 6 {
            return UIColor.gray
        }
        
        var rgbValue: UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func showCommanAlert(title: String, message: String) {
        let refreshAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_: UIAlertAction!) in
            
            refreshAlert.dismiss(animated: true, completion: nil)
        }))
        present(refreshAlert, animated: true, completion: nil)
    }
    
    func showloginpage(title: String, message: String) {
        let refreshAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_: UIAlertAction!) in
            refreshAlert.dismiss(animated: true, completion: nil)
            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "login") as! Login
            self.present(secondViewController, animated: true, completion: nil)
        }))
        present(refreshAlert, animated: true, completion: nil)
    }
    
    func showWarningToast(title: String, message: String) {
        DispatchQueue.main.async {
            let warning = MessageView.viewFromNib(layout: .cardView)
            warning.configureTheme(.error)
            warning.configureDropShadow()
            // let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
            warning.configureContent(title: title, body: message)
            warning.button?.isHidden = true
            var warningConfig = SwiftMessages.defaultConfig
            warningConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            warningConfig.presentationStyle = .center
            SwiftMessages.show(config: warningConfig, view: warning)
        }
    }
    
    func loginpost(apiDelegate: APIDelegate, requestCode: Int, url: String, params: [String: Any], isIndicatorShow: Bool) {
        if Reachability.isConnectedToNetwork() {
            print("Internet Connection Available!")
            
            print("parameters = \(params)")
            print("url = \(url)")
            if isIndicatorShow {
                DispatchQueue.main.async {
                    let attributes = RappleActivityIndicatorView.attribute(style: RappleStyle.circle, tintColor: .white, progreeBarFill: .white, thickness: 5)
                    RappleActivityIndicatorView.startAnimatingWithLabel(Localization("Contract_Processing"), attributes: attributes)
                }
            }
            //let header = UserDefaults.standard.string(forKey: "accessToken")
            
            var request = URLRequest(url: URL(string: url)!)
            request.httpMethod = "POST"
            request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            //request.addValue("Bearer " + header!, forHTTPHeaderField: "Authorization")
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                DispatchQueue.main.async {
                    RappleActivityIndicatorView.stopAnimation()
                }
                
                if error != nil {
                    self.showCommanAlert(title: "Error", message: "\(error)")
                } else {
                    // print("response: \(response)")
                    
                    let status = (response as! HTTPURLResponse).statusCode
                    print("response status: \(status)")
                    
                    if status == 200 || status == 201 {
                        if let safeData = data {
                            do {
                                let json = try JSON(data: safeData)
                                // print("api response\(json)")
                                apiDelegate.apiResponse(response: json, requestCode: requestCode)
                            } catch {
                                self.showCommanAlert(title: "Error", message: "\(error)")
                            }
                        } else {
                            self.showCommanAlert(title: "Error", message: "Unknown response from server.")
                        }
                        
                    } else if status == 401 || status == 400 {
                        if let safeData = data {
                            do {
                                let json = try JSON(data: safeData)
                                print("api response\(json)")
                                let obj = json.dictionaryValue
                                if let msg = obj["message"]?.string {
                                    DispatchQueue.main.async {
                                        self.showCommanAlert(title: "Error", message: msg)
                                    }
                                }
                                
                            } catch {
                                
                            }
                        } else {
                            self.showCommanAlert(title: "Error", message: "Unknown response from server.")
                        }
                        
                    } else {
                        DispatchQueue.main.async {
                            if let safeData = data {
                                do {
                                    let json = try JSON(data: safeData)
                                    print("api response\(json)")
                                    let obj = json.dictionaryValue
                                    if let msg = obj["message"]?.string {
                                        self.showCommanAlert(title: "Error", message: msg)
                                    } else {
                                        self.showCommanAlert(title: "Error", message: "Server Error try after some time")
                                    }
                                } catch {
                                    self.showCommanAlert(title: "Error", message: "Server Error try after some time")
                                }
                            } else {
                                self.showCommanAlert(title: "Error", message: "Server Error try after some time")
                            }
                        }
                    }
                }
            }
            task.resume()
        } else {
            showCommanAlert(title: "Error", message: "Internet Connection not Available!")
        }
    }
    
    
    
    func postRequest(apiDelegate: APIDelegate, requestCode: Int, url: String, params: [String: Any], isIndicatorShow: Bool) {
        if Reachability.isConnectedToNetwork() {
            print("Internet Connection Available!")
            print("parameters = \(params)")
            print("url = \(url)")
            if isIndicatorShow {
                DispatchQueue.main.async {
                    let attributes = RappleActivityIndicatorView.attribute(style: RappleStyle.circle, tintColor: .white, progreeBarFill: .white, thickness: 5)
                    RappleActivityIndicatorView.startAnimatingWithLabel(Localization("Contract_Processing"), attributes: attributes)
                }
            }
            let header = UserDefaults.standard.string(forKey: "accessToken")
            var request = URLRequest(url: URL(string: url)!)
            request.httpMethod = "POST"
            request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("Bearer \( header!)", forHTTPHeaderField: "Authorization")
            request.addValue("\(getCurrentLanguage())", forHTTPHeaderField: "Accept-Language")

print(request)
            
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                DispatchQueue.main.async {
                    RappleActivityIndicatorView.stopAnimation()
                }
                
                if error != nil {
                    self.showCommanAlert(title: "Error", message: "\(error)")
                } else {
                    // print("response: \(response)")
                    
                    let status = (response as! HTTPURLResponse).statusCode
                    print("response status: \(status)")
                    
                    if status == 200 || status == 201 {
                        if let safeData = data {
                            do {
                                let json = try JSON(data: safeData)
                                // print("api response\(json)")
                                apiDelegate.apiResponse(response: json, requestCode: requestCode)
                            } catch {
                                self.showCommanAlert(title: "Error", message: "\(error)")
                            }
                        } else {
                            self.showCommanAlert(title: "Error", message: "Unknown response from server.")
                        }
                        
                    } else if status == 401 || status == 400 {
                        if let safeData = data {
                            do {
                                let json = try JSON(data: safeData)
                                print("api response \(json)")
                                let obj = json.dictionaryValue
                                if let msg = obj["message"]?.string {
                                    DispatchQueue.main.async {
                                        self.showCommanAlert(title: "Error", message: msg)
                                    }
                                }
                                
                            } catch {
                                
                            }
                        } else {
                            self.showCommanAlert(title: "Error", message: "Unknown response from server.")
                        }
                        
                    } else {
                        DispatchQueue.main.async {
                            if let safeData = data {
                                do {
                                    let json = try JSON(data: safeData)
                                    print("api response\(json)")
                                    let obj = json.dictionaryValue
                                    if let msg = obj["message"]?.string {
                                        self.showCommanAlert(title: "Error", message: msg)
                                    } else {
                                        self.showCommanAlert(title: "Error", message: "Server Error try after some time")
                                    }
                                } catch {
                                    self.showCommanAlert(title: "Error", message: "Server Error try after some time")
                                }
                            } else {
                                self.showCommanAlert(title: "Error", message: "Server Error try after some time")
                            }
                        }
                    }
                }
            }
            task.resume()
        } else {
            showCommanAlert(title: "Error", message: "Internet Connection not Available!")
        }
    }
    
    func getRequest(apiDelegate: APIDelegate, requestCode: Int, url: String, params: [String: Any], isIndicatorShow: Bool) {
        if Reachability.isConnectedToNetwork() {
            print("Internet Connection Available!")
            
            print("parameters = \(params)")
            if isIndicatorShow {
                DispatchQueue.main.async {
                    let attributes = RappleActivityIndicatorView.attribute(style: RappleStyle.circle, tintColor: .white, progreeBarFill: .white, thickness: 5)
                    RappleActivityIndicatorView.startAnimatingWithLabel(Localization("Contract_Processing"), attributes: attributes)
                }
            }
            
            let header = UserDefaults.standard.string(forKey: "accessToken")
            print("token = ",header)
            let headers = ["Authorization": "Bearer "+header! , "Accept-Language" : "\(getCurrentLanguage())"]
            
            Alamofire.request(url, method: .get, parameters: params, headers: headers)
                .responseJSON { response in
                    let statusCode = (response.response?.statusCode)! // Gets HTTP status code, useful for debugging
                    print(statusCode)
                    // print(response)
                    
                    if statusCode == 200 {
                        if let value = response.result.value {
                            // Handle the results as JSON
                            let post = JSON(value)
                            apiDelegate.apiResponse(response: post, requestCode: requestCode)
                        }
                    }   else {
                        print(response)
                        DispatchQueue.main.async {
                            self.showCommanAlert(title: "Error", message: "Bad Credential or something went wrong!")
                        }
                    }
                    if isIndicatorShow{
                        DispatchQueue.main.async {
                            RappleActivityIndicatorView.stopAnimation()
                        }
                    }
            }
        } else {
            if isIndicatorShow{
                DispatchQueue.main.async {
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            DispatchQueue.main.async {
                self.showCommanAlert(title: "Error", message: "Internet Connection not Available!")
            }
            
        }
    }
    
    func putRequest(apiDelegate: APIDelegate, requestCode: Int, url: String, params: [String: Any]) {
        if Reachability.isConnectedToNetwork() {
            print("Internet Connection Available!")
            
            print("parameters = \(JSON(params))")
            print("url = \(url)")
            DispatchQueue.main.async {
                let attributes = RappleActivityIndicatorView.attribute(style: RappleStyle.circle, tintColor: .white, progreeBarFill: .white, thickness: 5)
                RappleActivityIndicatorView.startAnimatingWithLabel(Localization("Contract_Processing"), attributes: attributes)
            }
            
            let token: String = "Bearer "+UserDefaults.standard.string(forKey: "accessToken")!
            var request = URLRequest(url: URL(string: url)!)
            request.httpMethod = "PUT"
            do{
                request.httpBody = try JSON(params).rawData()
            }catch{
                print(error)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue(token, forHTTPHeaderField: "Authorization")
            request.addValue("\(getCurrentLanguage())", forHTTPHeaderField: "Accept-Language")
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                DispatchQueue.main.async { RappleActivityIndicatorView.stopAnimation() }
                if error != nil {
                    self.showCommanAlert(title: "Error", message: "\(error)")
                } else {
                    let status = (response as! HTTPURLResponse).statusCode
                    print("response status: \(status)")
                    
                    if status == 200 {
                        if let safeData = data {
                            do {
                                let json = try JSON(data: safeData)
                                print("api response\(json)")
                                apiDelegate.apiResponse(response: json, requestCode: requestCode)
                            } catch {
                                self.showCommanAlert(title: "Error", message: "\(error)")
                            }
                        } else {
                            self.showCommanAlert(title: "Error", message: "Unknown response from server.")
                        }
                    } else {
                        if let safeData = data {
                            do {
                                let json = try JSON(data: safeData)
                                print("api response \(json)")
                                let obj = json.dictionaryValue
                                if let msg = obj["message"]?.string {
                                    DispatchQueue.main.async {
                                        self.showCommanAlert(title: "Error", message: msg)
                                    }
                                }
                                
                            } catch {
                                
                            }
                        } else {
                            self.showCommanAlert(title: "Error", message: "Unknown response from server.")
                        }
                    }
                }
            }
            task.resume()
        } else {
            showCommanAlert(title: "Error", message: "Internet Connection not Available!")
        }
    }
    
    @objc func updateclock() -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm:ss a"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    func buttonanim(sender: UIView) {
        UIView.animate(withDuration: 0.1,
                       animations: {
                        sender.transform = CGAffineTransform(scaleX: 0.975, y: 0.96)
        },
                       completion: { _ in
                        UIView.animate(withDuration: 0.1, animations: {
                            sender.transform = CGAffineTransform.identity
                        })
        })
    }
    
    func getDayOfWeek(_ today: String) -> Int? {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekOfYear, from: todayDate)
        return weekDay
    }
    
    func getDateFromTimestamp(timestamp: Double) -> String {
        let dateTimeStamp = NSDate(timeIntervalSince1970: Double(timestamp) / 1000) // UTC time
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local // Edit
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
        
        return strDateSelect
    }
    
    func getDateandTimeFromTimestamp(timestamp: Double) -> String {
        let dateTimeStamp = NSDate(timeIntervalSince1970: Double(timestamp) / 1000) // UTC time
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local // Edit
        dateFormatter.dateFormat = "hh:mm a"
        
        let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
        return strDateSelect
    }
    
    func getdurationfromtimestamp(timestamp: Double) -> String {
        let dateTimeStamp = NSDate(timeIntervalSince1970: Double(timestamp) / 1000) // UTC time
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local // Edit
        dateFormatter.dateFormat = "hh:mm:ss"
        
        let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
        return strDateSelect
    }
    
    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "hh:mm:ss"
        
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            
            return outputFormatter.string(from: date)
        }
        
        return nil
    }
    
    func convertstringtodate(dateString: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let date = dateFormatter.date(from: dateString)
        return date
    }
    
    func daysBetween(start: Date, end: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: start, to: end).day!
    }
    
    func isValidEmail(testStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func getCurrentDate(format: String) -> String {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: Date())
        
    }
    
   func  getcurrentTime() -> String {
           let formatter = DateFormatter()
           formatter.dateFormat = "hh:mm:ss"
           return formatter.string(from: Date())
       }
    
    func getagodates(format: String,days: Int)-> String {
        let today = Date()
        let thirtyDaysBeforeToday = Calendar.current.date(byAdding: .day, value: days, to: today)!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: thirtyDaysBeforeToday)
    }
    
    
    
    func getweekstartdate(days: Int)-> Date {
        let gregorian = Calendar(identifier: .gregorian)
        let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: Date()))
        return gregorian.date(byAdding: .day, value: days, to: sunday!)!
        
        
    }
    
    
    
    
    
    func datePickerTapped(textField: UILabel,format: String,check: String) {
        DatePickerDialog().show(Localization("Contract_DatePicker"), doneButtonTitle: Localization("Contract_Done"), cancelButtonTitle: Localization("Contract_Cancel"), datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                textField.text = formatter.string(from: dt)
                var fromvalue = formatter.string(from: dt)
                print("from value = ",fromvalue)
                
            }
        }
    }
    func datePickerTapped_TextFiled(textField: UITextField,format: String,check: String) {
        DatePickerDialog().show(Localization("Contract_DatePicker"), doneButtonTitle: Localization("Contract_Done"), cancelButtonTitle: Localization("Contract_Cancel"), datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                textField.text = formatter.string(from: dt)
                var fromvalue = formatter.string(from: dt)
                print("from value = ",fromvalue)
                
            }
        }
    }
    
    func showindicator()  {
        DispatchQueue.main.async {
            let attributes = RappleActivityIndicatorView.attribute(style: RappleStyle.circle, tintColor: .white, progreeBarFill: .white, thickness: 7)
            RappleActivityIndicatorView.startAnimatingWithLabel(Localization("Contract_Processing"), attributes: attributes)
        }
    }
    
    func showdialog(msg:String,body:String) {
        let refreshAlert = UIAlertController(title: msg, message: body, preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: Localization("Contract_Ok"), style: .default, handler: { (_: UIAlertAction!) in
            refreshAlert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(refreshAlert, animated: true, completion: nil)
    }
    
    
    
}





extension String {
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self, options: Data.Base64DecodingOptions(rawValue: 0)) else {
            return nil
        }
        
        return String(data: data as Data, encoding: String.Encoding.utf8)
    }
    
    func toBase64() -> String? {
        guard let data = self.data(using: String.Encoding.utf8) else {
            return nil
        }
        
        let dt = data.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        print("base 64 = \(dt)")
        return dt
    }
}
extension UIView{
    
    func customActivityIndicator(view: UIView, widthView: CGFloat?,backgroundColor: UIColor?, textColor:UIColor?, message: String?) -> UIView{
        let animationLayer : CALayer = {
            return CALayer()
        }()
        //Config UIView
        self.backgroundColor = backgroundColor //Background color of your view which you want to set
        
        var selfWidth = view.frame.width
        if widthView != nil{
            selfWidth = widthView ?? selfWidth
        }
        
        let selfHeigh = view.frame.height
        let loopImages = UIImageView()
        let loopImageslogo = UIImageView()
        loopImageslogo.image = UIImage(named: "logo")!
        let imageListArray = [UIImage(named: "loading.png")!] // Put your desired array of images in a specific order the way you want to display animation.
        loopImages.animationImages = imageListArray
        loopImages.animationDuration = TimeInterval(0.8)
        loopImages.startAnimating()
        let rotation : CABasicAnimation = CABasicAnimation(keyPath:"transform.rotation.z")
        
        rotation.duration = 1.0
        rotation.isRemovedOnCompletion = false
        rotation.repeatCount = HUGE
        rotation.fillMode = CAMediaTimingFillMode.forwards
        rotation.fromValue = NSNumber(value: 0.0)
        rotation.toValue = NSNumber(value: 3.14 * 2.0)
        
        animationLayer.frame = frame
        animationLayer.contents = loopImages.image?.cgImage
        animationLayer.masksToBounds = true
        
        self.layer.addSublayer(animationLayer)
        
        loopImages.layer.add(rotation, forKey: "rotate")
        let imageFrameX = (selfWidth / 2) - 30
        let imageFrameY = (selfHeigh / 2) - 60
        var imageWidth = CGFloat(60)
        var imageHeight = CGFloat(60)
        
        if widthView != nil{
            imageWidth = widthView ?? imageWidth
            imageHeight = widthView ?? imageHeight
        }
        
        
        
        //ConfigureLabel
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .gray
        label.numberOfLines = 0
        label.text = message ?? ""
        label.textColor = textColor ?? UIColor.clear
        
        //Config frame of label
        let labelFrameX = (selfWidth / 2) - 100
        let labelFrameY = (selfHeigh / 2) - 10
        let labelWidth = CGFloat(200)
        let labelHeight = CGFloat(70)
        
        // Define UIView frame
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width , height: UIScreen.main.bounds.size.height)
        
        
        //ImageFrame
        loopImages.frame = CGRect(x: imageFrameX, y: imageFrameY, width: imageWidth, height: imageHeight)
        loopImageslogo.frame = CGRect(x: imageFrameX + 10, y: imageFrameY + 10, width: imageWidth - 20, height: imageHeight - 20)
        loopImageslogo.cornerRadiusV = 20
        //LabelFrame
        label.frame = CGRect(x: labelFrameX, y: labelFrameY, width: labelWidth, height: labelHeight)
        
        //add loading and label to customView
        self.addSubview(loopImages)
        self.addSubview(loopImageslogo)
        self.addSubview(label)
        return self
        
    }
    
    
    
}

func getCurrentLanguage() -> String {
    if(UserDefaults.standard.value(forKey: "Contract_Language_EN") != nil){
        if(UserDefaults.standard.value(forKey: "Contract_Language_EN")as! Bool == true){
            return "en"
        }else{
            return "es-mx"
        }
    }
    return "en"
}
