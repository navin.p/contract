//
//  UIView.swift
//  Contrack
//
//  Created by Saavan Patidar on 16/12/20.
//  Copyright © 2020 Vikram Singh. All rights reserved.
//

import UIKit

extension UIView {
@IBInspectable var cornerRadiusV: CGFloat {
get {
return layer.cornerRadius
}
set {
layer.cornerRadius = newValue
layer.masksToBounds = newValue > 0
}
}

@IBInspectable var borderWidthV: CGFloat {
get {
return layer.borderWidth
}
set {
layer.borderWidth = newValue
}
}

@IBInspectable var borderColorV: UIColor? {
get {
return UIColor(cgColor: layer.borderColor!)
}
set {
layer.borderColor = newValue?.cgColor
}
}
}
