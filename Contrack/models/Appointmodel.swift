

class Appointmodel: Codable {
    var proimage = "" ,proaddress = "", schedule = "", technician = "", status = "", crewid = "", workorderid = "", workorderdate = "", imagename = "", imageid = "",proname = ""
    init(proimage: String, proaddress: String,schedule: String,technician: String,status: String
        ,crewid: String,workorderid: String,workorderdate: String,imagename:String,imageid:String,proname:String) {
        self.proimage = proimage
        self.proaddress = proaddress
        self.schedule = schedule
        self.technician = technician
        self.status = status
        self.crewid = crewid
        self.workorderid = workorderid
        self.workorderdate = workorderdate
        self.imagename = imagename
        self.imageid = imageid
        self.proname = proname
    }
}
