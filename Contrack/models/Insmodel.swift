import UIKit

class Insmodel{
    var id : Int
    var name = ""
    var path = ""
    var  image = UIImage()
    init(id: Int, name: String,path: String,image: UIImage) {
        self.id = id
        self.name = name
        self.path = path
        self.image = image
        
    }
}
