//
//  AppDelegate.swift
//  Contrack
//
//  Created by Vikram Singh on 05/10/20.
//  Copyright © 2020 Vikram Singh. All rights reserved.
//

import UIKit
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

 var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        if #available(iOS 13.0, *) {
            UIWindow.appearance().overrideUserInterfaceStyle = .light
        }

        return true
    }
    
    func applicationDidFinishLaunching(_ application: UIApplication) {
        
    }
                                                       
}

