//
//  LanguageSelectionVCViewController.swift
//  Contrack
//
//  Created by Navin Patidar on 10/12/21.
//  Copyright © 2021 Vikram Singh. All rights reserved.
//

import UIKit
import DLRadioButton



class LanguageSelectionVC: UIViewController {

    @IBOutlet weak var Rb_English: DLRadioButton!
    @IBOutlet weak var Rb_Spanish: DLRadioButton!
    @IBOutlet weak var btn_Back: UIButton!
    @IBOutlet weak var btn_English: UIButton!
    @IBOutlet weak var btn_Spanish: UIButton!
    @IBOutlet weak var lbl_SelectLanguage: UILabel!
    @IBOutlet weak var lbl_SelectLanguageTitle: UILabel!
    @IBOutlet weak var btn_Save: UIButton!
    @IBOutlet weak var img_Logo: UIImageView!

    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
    var strComeFrom = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewFromLocalisation()
        Rb_English.isSelected = true
        Rb_English.addTarget(self, action: #selector(radioclickEnglish), for: UIControl.Event.touchUpInside)
        Rb_Spanish.addTarget(self, action: #selector(radioclickSpanish), for: UIControl.Event.touchUpInside)
        if(strComeFrom == "Update_Language"){
            btn_Back.isHidden = false
        }else{
            btn_Back.isHidden = true
        }
        Rb_English.isSelected = true
        Rb_Spanish.isSelected = false
      
        if(UserDefaults.standard.value(forKey: "Contract_Language_EN") != nil){
            if(UserDefaults.standard.value(forKey: "Contract_Language_EN") as! Bool){
                Rb_English.isSelected = true
                Rb_Spanish.isSelected = false
            }else{
                Rb_English.isSelected = false
                Rb_Spanish.isSelected = true
            }
        }
    }
    
    @objc func radioclickEnglish(sender: UIButton!) {
        isEnglish = true
        Rb_English.isSelected = true
        Rb_Spanish.isSelected = false
        if SetLanguage(arrayLanguages[1]) {}
        UserDefaults.standard.setValue(isEnglish , forKey: "Contract_Language_EN")
        configureViewFromLocalisation()
    }
    
    @objc func radioclickSpanish(sender: UIButton!) {
        isEnglish = false
        Rb_English.isSelected = false
        Rb_Spanish.isSelected = true
        if SetLanguage(arrayLanguages[0]) {}
        UserDefaults.standard.setValue(isEnglish , forKey: "Contract_Language_EN")
        configureViewFromLocalisation()

    }

    @IBAction func action_Back(_ sender: Any) {
        self.dismiss(animated: false) {}

    }
    
    @IBAction func action_Save(_ sender: Any) {
        UserDefaults.standard.setValue(isEnglish , forKey: "Contract_Language_EN")

        if(strComeFrom == "Update_Language"){
            self.dismiss(animated: false) {}
        }else{
            let loginvc = storyboard?.instantiateViewController(withIdentifier: "login") as! Login
            loginvc.modalPresentationStyle = .fullScreen
            present(loginvc, animated: true, completion: nil)
        }
    }
}
//MARK:
//MARK: -----------Localisation Function

extension LanguageSelectionVC{
    
    func configureViewFromLocalisation() {
        
        if(UserDefaults.standard.value(forKey: "Contract_Language_EN") != nil){
            if(UserDefaults.standard.value(forKey: "Contract_Language_EN") as! Bool){
                if SetLanguage(arrayLanguages[1]) { }
                isEnglish = true
            }else{
                if SetLanguage(arrayLanguages[0]) { }
                isEnglish = false
            }
        }else{
            if SetLanguage(arrayLanguages[1]) { }
            isEnglish = true
        }

        
        if(isEnglish){
            img_Logo.image = UIImage(named: "logo1")
        }else{
            img_Logo.image = UIImage(named: "logo1")
        }
        
        
        btn_Spanish.setTitle(Localization("Contract_Spanish"), for: .normal)
        btn_English.setTitle(Localization("Contract_English"), for: .normal)
        lbl_SelectLanguage.text =  Localization("Contract_SelectLanguage")
        lbl_SelectLanguageTitle.text =  Localization("Contract_SelectLanguage")
        btn_Save.setTitle(Localization("Contract_Save"), for: .normal)
    }
}
