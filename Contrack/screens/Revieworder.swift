//
//  Revieworder.swift
//  Contrack
//
//  Created by Vikram Singh on 09/11/20.
//  Copyright © 2020 Vikram Singh. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

class Revieworder: Common,APIDelegate {
    
    
    @IBOutlet weak var lbl_S_Title: UILabel!
    @IBOutlet weak var lbl_S_WOInfo: UILabel!

    @IBOutlet weak var lbl_S_CrewMember: UILabel!
    @IBOutlet weak var lbl_S_TotalTime: UILabel!
    @IBOutlet weak var lbl_S_TaskChecked: UILabel!
    @IBOutlet weak var lbl_S_TaskList: UILabel!
    @IBOutlet weak var lbl_S_Comment: UILabel!
    @IBOutlet weak var lbl_S_BeforePic: UILabel!
    @IBOutlet weak var lbl_S_AfterPic: UILabel!

    @IBOutlet weak var Img_review: UIImageView!
    @IBOutlet weak var Lb_propertyname: UILabel!
    @IBOutlet weak var Lb_comment: UILabel!
    @IBOutlet weak var Lb_name: UILabel!
    
    @IBOutlet weak var Bt_submit: UIButton!
    @IBOutlet weak var stack_after: UIStackView!
    @IBOutlet weak var satck_before: UIStackView!
    @IBOutlet weak var stack_tasklist: UIStackView!
    @IBOutlet weak var Bt_totaltime: UIButton!
    @IBOutlet weak var Bt_totalcrew: UIButton!
    @IBOutlet weak var Lb_workorder3: UILabel!
    @IBOutlet weak var Lb_workorder2: UILabel!
    @IBOutlet weak var Lb_workorder1: UILabel!
    @IBOutlet weak var Lb_email: UILabel!
    @IBOutlet weak var Lb_contact: UILabel!
    @IBOutlet weak var Lb_address: UILabel!
    var workorderobj1 = [String:Any]()
    var workorderid = "",checkforstatus = "",checkintime = "",checkouttime = ""
    var tasklist = [Taskmodel]()
    var sendstatus = false
    var strlat = "" , strLong = "" , checkOutTime = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewFromLocalisation()
        modalPresentationCapturesStatusBarAppearance = true
        setDataonView()

        DispatchQueue.main.async {
            let url = "\(self.BASE_URL)workorder?workOrderId="+self.workorderid
            print("url = ",url)
            self.getRequest(apiDelegate: self, requestCode: 1, url: url, params: [:], isIndicatorShow: true)
        }
        LocationManager.shared.getLocation { (location:CLLocation?, error:NSError?) in
            
            if error != nil {
                self.alertMessage(message: (error?.localizedDescription)!, buttonText: Localization("Contract_Ok"), completionHandler: nil)
                return
            }
            guard let location = location else {
                self.alertMessage(message: Localization("Contract_fetchLocation"), buttonText: Localization("Contract_Ok"), completionHandler: nil)
                return
            }
            self.strlat = "\(location.coordinate.latitude)"
            self.strLong = "\(location.coordinate.longitude)"

            print("-----------\(location.coordinate.latitude)")
            print("-----------\(location.coordinate.longitude)")

         

        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.first
            let topPadding = window?.safeAreaInsets.top
            let statusBar = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: topPadding ?? 0.0))
            statusBar.backgroundColor = Revieworder.hexStringToUIColor(hex: appThemeColor)
            UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.addSubview(statusBar)
        }
    }
    
    func alertMessage(message:String,buttonText:String,completionHandler:(()->())?) {
        let alert = UIAlertController(title: Localization("Contract_Location"), message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: buttonText, style: .default) { (action:UIAlertAction) in
            completionHandler?()
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func submitClick(_ sender: Any) {
        if (sendstatus){
            showconfimationdialog(title: Localization("Contract_TaskList"), msg: Localization("Contract_partiallycompletedAlert"))
            
            
        }else{
            showconfimationdialog(title: Localization("Contract_TaskList"), msg: Localization("Contract_completedAlert"))
        }
        
    }
    
    @IBAction func backClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func updateworkorder(timeout:String,status:String){
        let url = "\(self.BASE_URL)workorderupdate"
        print("url = ",url)
     //   workorderobj1.updateValue(timeout, forKey: "timeOut")
        workorderobj1.updateValue(status, forKey: "status")
        
        self.workorderobj1["timeInLatitude"] = "\(self.strLat)"
        self.workorderobj1["timeOutLatitude"] = "\(self.strLong)"
        
        
        putRequest(apiDelegate: self, requestCode: 2, url: url, params: workorderobj1)
    }
    
    
    func showconfimationdialog(title: String, msg: String) {
        let refreshAlert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: Localization("Contract_Cancel"), style: .default, handler: { (_: UIAlertAction!) in
      
        }))
        refreshAlert.addAction(UIAlertAction(title: Localization("Contract_Ok"), style: .default, handler: { (_: UIAlertAction!) in
            var status = ""
            if (self.sendstatus){
                status = "partially_complete"
            }else {
                status = "complete"
            }
            let timeout = self.checkintime
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from:timeout)!
            let timeinMiliSecond = date.millisecondsSince1970 // 1476889390939

            self.updateworkorder(timeout:"\(timeinMiliSecond)", status:status)
            refreshAlert.dismiss(animated: true, completion: nil)
        }))
       
        self.present(refreshAlert, animated: true, completion: nil)
        
        
        
        
        
    }
    func setDataonView()  {
        
        
        checkOutTime = "\(workorderobj1["timeOut"] ?? "")"

          
        
        
    }
    
    func apiResponse(response: JSON, requestCode: Int) {
        if requestCode == 1 {
            print(response)
            DispatchQueue.main.async { [self] in
                var workorderobj = response.dictionaryValue
                self.workorderobj1 = response.dictionaryValue
                self.workorderobj1.updateValue(checkOutTime, forKey: "timeOut")
                workorderobj.updateValue(JSON(rawValue: checkOutTime) ?? "", forKey: "timeOut")

                
                
                var checkinttime1 = "\(workorderobj["timeIn"] ?? "")"
               // workorderobj["timeIn"]?.string ?? ""
                let checkouttime1 = "\(workorderobj["timeOut"] ?? "")"
                //workorderobj["timeOut"]?.string ?? ""
                self.checkforstatus = workorderobj["status"]?.string?.lowercased() as! String
                if (self.checkforstatus == "complete" || self.checkforstatus == "partially_complete"){
                    self.Bt_submit.isHidden = true
                }
                
                
             
            
                
                if (!checkinttime1.isEmpty && checkinttime1 != "null" && checkinttime1 != "") {
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let numberIN: Int64? = Int64(checkinttime1)
                    let Checkin = dateFormatter.string(from: Date(milliseconds: numberIN ?? 0))
                    print(Checkin)
                    
                    let numberOut: Int64? = Int64(checkouttime1)
                    let CheckOut = dateFormatter.string(from: Date(milliseconds: numberOut ?? 0))
                    print(CheckOut)
                    
                    self.checkintime = Checkin
                    self.checkouttime = CheckOut
                    print("checkin time = ", self.checkintime)
                    print("current time = ", checkouttime)
                    
                    let date1 = dateFormatter.date(from: self.checkintime)!
                    let date2 = dateFormatter.date(from: checkouttime)!
                    let components = Set<Calendar.Component>([.minute, .hour])
                    let differenceOfDate = Calendar.current.dateComponents(components, from: date1, to: date2)
                    let datestr = "\(String(differenceOfDate.hour!)):\(String(differenceOfDate.minute!)) hours"
                    print("checkouttime = ", datestr)
                    self.Bt_totaltime.setTitle(datestr, for: .normal)
                    
                }
                self.Lb_propertyname.text = workorderobj["propertyName"]?.string
                self.Lb_name.text = workorderobj["propertyManagerName"]?.string
                self.Lb_contact.text = "Contact - "+(workorderobj["propertyManagerCellNo"]?.string)!
                self.Lb_email.text = "Email - "+(workorderobj["propertyEmailid"]?.string)!
                self.Lb_address.text = workorderobj["propertyAddress"]?.string ?? ""
                self.Lb_workorder1.text = "Workorder :     "+self.workorderid
                self.Lb_workorder2.text = "Work Date :     "+(workorderobj["workOrderDate"]?.string)!
                var concat = (workorderobj["crewFirstName"]?.string!)!+" "+(workorderobj["crewLastName"]?.string!)!
                self.Lb_workorder3.text = "Technician :     "+concat
                self.Lb_comment.text = workorderobj["techComment"]?.string ?? ""
                var totalcrew = workorderobj["totalCrewMember"]?.int ?? 0
                self.Bt_totalcrew.setTitle("\(totalcrew)", for: .normal)
                var jsonArray = workorderobj["propertyImage"]?.array
                for i in jsonArray!{
                    if let str = i["imageData"].string {
                        let url = URL(string: str)
                        print("url = ",url)
                        self.Img_review.kf.setImage(with: url)
                    }
                }
                var taskarr = workorderobj["workOrderTaskList"]?.array
                var index = 0
                if(taskarr!.count > 0){
                    for j in taskarr!{
                        print("tasklist tag = ",index)
                        var taskname = "" , instruction = "", status = "",taskid = ""
                        if let str = j["taskId"].int{
                            taskid = String(str)
                        }
                        if let str = j["taskName"].string{
                            taskname = str
                        }
                        if let str = j["serviceInstruction"].string{
                            instruction = str
                        }
                        if let str = j["status"].string?.lowercased(){
                            status = str
                        }
                        var temp =
                            Taskmodel(tastid: taskid,
                                      taskName:  taskname,
                                      serviceInstruction: instruction,
                                      status:  status,
                                      checkforarrow: false,
                                      status1: status
                        )
                        
                        if (status == "incomplete"){
                            self.sendstatus = true
                        }
                        
                        
                        self.tasklist.append(temp)
                        
                    }
                }
                self.loadtasklist()
                var berforeimagearr = workorderobj["beforeImage"]?.array
                if (berforeimagearr!.count > 0) {
                    for v in self.satck_before.subviews {
                        v.removeFromSuperview()
                    }
                    
                    for k in berforeimagearr!{
                        let url = URL(string: k["imageData"].string ?? "")
                        print("url = ",url)
                        let tempView1 = Bundle.main.loadNibNamed("Beforepicture", owner: self, options: nil)!.first as? Beforepicture
                        tempView1?.layoutIfNeeded()
                        tempView1?.layoutSubviews()
                        tempView1?.beforeimage.tag = index
                        tempView1?.deleteBtn.tag = index
                        tempView1?.imageBtn.tag = index
                        tempView1?.deleteBtn.isHidden = true
                        tempView1?.beforeimage.kf.setImage(with: url)
                           tempView1?.widthAnchor.constraint(equalToConstant: (tempView1?.mainView.frame.width)!+10).isActive = true
                        tempView1?.layoutIfNeeded()
                        self.satck_before.addArrangedSubview(tempView1!)
                        self.satck_before.layoutSubviews()
                        
                        
                    }
                    
                }
                
                var afterimagearr = workorderobj["afterImage"]?.array
                if (afterimagearr!.count > 0) {
                    for v in self.stack_after.subviews {
                        v.removeFromSuperview()
                    }
                    
                    for k in afterimagearr!{
                        let url = URL(string: k["imageData"].string ?? "")
                        print("url = ",url)
                        
                        
                        
                        let tempView1 = Bundle.main.loadNibNamed("Beforepicture", owner: self, options: nil)!.first as? Beforepicture
                        tempView1?.layoutIfNeeded()
                        tempView1?.layoutSubviews()
                        tempView1?.beforeimage.tag = index
                        tempView1?.deleteBtn.tag = index
                        tempView1?.imageBtn.tag = index
                        tempView1?.deleteBtn.isHidden = true
                        tempView1?.beforeimage.kf.setImage(with: url)
                           tempView1?.widthAnchor.constraint(equalToConstant: (tempView1?.mainView.frame.width)!+10).isActive = true
                        tempView1?.layoutIfNeeded()
                        self.stack_after.addArrangedSubview(tempView1!)
                        self.stack_after.layoutSubviews()
                        
                        
                    }
                    
                }
                
                
                
                
                
                
            }
        }
        
        if requestCode == 2 {
            print(response)
            var jsonobject = response.dictionaryValue
            DispatchQueue.main.async {
                let refreshAlert = UIAlertController(title: Localization("Contract_Success"), message: jsonobject["message"]?.string, preferredStyle: UIAlertController.Style.alert)
                
                refreshAlert.addAction(UIAlertAction(title: Localization("Contract_Ok"), style: .default, handler: { (_: UIAlertAction!) in
                 let appointVc = self.storyboard?.instantiateViewController(withIdentifier: "appointmentlist") as! Appointmentlist
                    appointVc.modalPresentationStyle = .fullScreen
                    self.present(appointVc, animated: true, completion: nil)
                    
                    refreshAlert.dismiss(animated: true, completion: nil)
                }))
                
                self.present(refreshAlert, animated: true, completion: nil)
            }
        }
        
        
    }
    
    
    func loadtasklist(){
        for v in stack_tasklist.subviews {
            v.removeFromSuperview()
        }
        for(index,element) in tasklist.enumerated(){
            let tempView1 = Bundle.main.loadNibNamed("Inprogresstasklist", owner: self, options: nil)!.first as? Inprogresstasklist
            tempView1?.layoutIfNeeded()
            tempView1?.layoutSubviews()
            
            tempView1?.Chkbox_task.tag = index
            tempView1?.Lb_checkbox.text = element.taskName
            var status = element.status.lowercased()
            if (status == "complete") {
                tempView1?.Chkbox_task.isSelected = true
            }
            
            tempView1?.Chkbox_task.addTarget(self, action: #selector(self.checkboxclick), for: UIControl.Event.touchUpInside)
            
            tempView1?.heightAnchor.constraint(equalToConstant: (tempView1?.stack_holder.frame.height)!+10).isActive = true
            tempView1?.layoutIfNeeded()
            self.stack_tasklist.addArrangedSubview(tempView1!)
            self.stack_tasklist.layoutSubviews()
            
        }
    }
    
    @objc func checkboxclick(sender: UIButton!) {
        var tag: Int = sender.tag
        print("tag is = ",tag)
        var status = tasklist[tag].status.lowercased()
        print("status = ",status)
        if (!status.elementsEqual("complete")) {
            sender.isSelected = false
        } else{
            sender.isSelected = true
        }
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor =  #colorLiteral(red: 0.0943563953, green: 0.2551338077, blue: 0.2927393913, alpha: 1)
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
        return .lightContent
    }
    
    
    
}


//MARK:
//MARK: -----------Localisation Function

extension Revieworder{
    
    func configureViewFromLocalisation() {
        lbl_S_Title.text =  Localization("Contract_Review")
        lbl_S_WOInfo.text =  Localization("Contract_WorkOrderInfo")
        lbl_S_CrewMember.text =  Localization("Contract_TotalCrewMembers")
        lbl_S_TotalTime.text =  Localization("Contract_TotalTime") + "*"
        lbl_S_TaskChecked.text =  Localization("Contract_CompleteTaskChecked")
        lbl_S_TaskList.text =  Localization("Contract_TaskList")

        lbl_S_Comment.text =  Localization("Contract_Comments")
        lbl_S_BeforePic.text =  Localization("Contract_BeforePicture")
        lbl_S_AfterPic.text =  Localization("Contract_AfterPicture")

        Bt_submit.setTitle(Localization("Contract_Submit"), for: .normal)



        
    }
}
