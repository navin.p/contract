//
//  Tasklist.swift
//  Contrack
//
//  Created by Vikram Singh on 05/11/20.
//  Copyright © 2020 Vikram Singh. All rights reserved.
//

import UIKit

class Tasklist: UIView {

    @IBOutlet weak var taskview: UIStackView!
    
    @IBOutlet weak var Bt_expand: UIButton!
    @IBOutlet weak var Lb_instruct: UILabel!
    @IBOutlet weak var Lb_task: UILabel!
    @IBOutlet weak var Bt_updown: UIButton!
}

