//
//  Generalinfo.swift
//  Contrack
//
//  Created by Vikram Singh on 04/11/20.
//  Copyright © 2020 Vikram Singh. All rights reserved.
//

import UIKit
import SwiftyJSON
import RappleProgressHUD
import ANZSingleImageViewer
import Alamofire
import WXImageCompress
import CoreLocation


class Generalinfo: Common,APIDelegate,UIImagePickerControllerDelegate{
    
    @IBOutlet weak var Lb_timer1: UILabel!
    @IBOutlet weak var Lb_date: UILabel!
    @IBOutlet weak var stack_tasklist: UIStackView!
    @IBOutlet weak var addphotoview: CardView!
    @IBOutlet weak var addwidth: NSLayoutConstraint!
    @IBOutlet weak var addheight: NSLayoutConstraint!
    @IBOutlet weak var Lb_propertyname: UILabel!
    @IBOutlet weak var Img_property: UIImageView!
    @IBOutlet weak var Lb_contact: UILabel!
    @IBOutlet weak var Lb_technician: UILabel!
    @IBOutlet weak var Bt_save: UIButton!
    @IBOutlet weak var Bt_checkin: UIButton!
    @IBOutlet weak var Lb_timer: UILabel!
    @IBOutlet weak var stack_before: UIStackView!
    @IBOutlet weak var Lb_count: UILabel!
    @IBOutlet weak var Lb_comment: UILabel!
    @IBOutlet weak var Lb_workorderdate: UILabel!
    @IBOutlet weak var Lb_workorder: UILabel!
    @IBOutlet weak var Lb_address: UILabel!
    @IBOutlet weak var Lb_email: UILabel!
    @IBOutlet weak var Lb_name: UILabel!
  
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_S_WorkOrderInfo: UILabel!
    @IBOutlet weak var lbl_S_WorkOrderComment: UILabel!
    @IBOutlet weak var lbl_S_CrewMember: UILabel!
    @IBOutlet weak var lbl_S_TaskList: UILabel!
    @IBOutlet weak var lbl_S_BeforeImage: UILabel!
    @IBOutlet weak var lbl_S_PleaseUploadMin: UILabel!

    
    var workorderid = "", checkintime = "",techcomment = "",totalCrewMember = "",status = "",checktimeout = ""
    var checkforcheckin = false, setfirsttime = true
    var tasklist = [Taskmodel]()
    var imagepathlist = [Insmodel]()
    var imagelist = [Any]()
    var workorderobj1 = [String:Any]()
    var imagecount = 0
    var timer = Timer()
    var strlat = "" , strLong = ""
    public static var checkstatus = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewFromLocalisation()
        print("status = ",status)
        print("workorderid = ",workorderid)
        Lb_date.text = getCurrentDate(format: "yyyy-MM-dd")
        //tick()
        getCurrentTime()
        if (!self.status.elementsEqual("incomplete")) {
            self.Bt_checkin.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.addphotoview.isHidden = true
            addwidth.constant = 0
            Lb_timer.isHidden = true
            Lb_timer1.isHidden = false
            
        }
        
        let url = "\(BASE_URL)workorder?workOrderId="+workorderid
        print("url = ",url)
        getRequest(apiDelegate: self, requestCode: 1, url: url, params: [:], isIndicatorShow: true)
       
        LocationManager.shared.getLocation { (location:CLLocation?, error:NSError?) in
            
            if error != nil {
                
                self.alertMessage(message: (error?.localizedDescription)!, buttonText: Localization("Contract_Ok"), completionHandler: nil)
                return
            }
            guard let location = location else {
                self.alertMessage(message: Localization("Contract_fetchLocation"), buttonText:  Localization("Contract_Ok"), completionHandler: nil)
                return
            }
            self.strlat = "\(location.coordinate.latitude)"
            self.strLong = "\(location.coordinate.longitude)"

            print("-----------\(location.coordinate.latitude)")
            print("-----------\(location.coordinate.longitude)")

        }
          
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.first
            let topPadding = window?.safeAreaInsets.top
            let statusBar = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: topPadding ?? 0.0))
            statusBar.backgroundColor = Generalinfo.hexStringToUIColor(hex: appThemeColor)
            UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.addSubview(statusBar)
        }
    }
    
    func alertMessage(message:String,buttonText:String,completionHandler:(()->())?) {
        let alert = UIAlertController(title: Localization("Contract_Location"), message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: buttonText, style: .default) { (action:UIAlertAction) in
            completionHandler?()
           // self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
       // locationManager.stopUpdatingLocation()
    }


     func SettingView() {
        let alertController = UIAlertController (title: Localization("Contract_LocationDisabled"), message: Localization("Contract_LocationMessage"), preferredStyle: .alert)

        let settingsAction = UIAlertAction(title: Localization("Contract_Settings"), style: .default) { (_) -> Void in

            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }

            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
     

        present(alertController, animated: true, completion: nil)
    }
    func imagePickerController(_: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            //profleImage.contentMode = .scaleAspectFit
            //profleImage.image = pickedImage
            //dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(picker _: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    private func getCurrentTime() {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(self.currentTime) , userInfo: nil, repeats: true)
    }
    
    @objc func currentTime() {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        Lb_timer.text = formatter.string(from: Date())
    }
    
    
    @IBAction func backClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func minusClick(_ sender: Any) {
        if (Generalinfo.checkstatus == "incomplete") {
            var counttxt = Lb_count.text!
            if (Int(counttxt)! > 1) {
                var countvalue = Int(counttxt)!
                var count = countvalue - 1
                totalCrewMember = String(count)
                Lb_count.text =  String(count)
            }
        }
    }
    
    @IBAction func plucClick(_ sender: Any) {
        if (Generalinfo.checkstatus == "incomplete") {
            var countvalue = Lb_count.text!
            if (Int(countvalue) == 0 || Int(countvalue)! > 0) {
                var count = Int(countvalue)!
                count += 1
                totalCrewMember = String(count)
                Lb_count.text = String(count)
            }
        }
        
    }
    
    
    
    @IBAction func addphotoClick(_ sender: Any) {
        ImagePickerManager().pickImage(self) { imageview in
            // here is the image
            //self.profleImage.image = imageview
            let date = Date()
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd hh:mm:ss"
            let dateString = df.string(from: date)
            //var filename = "image"+String(self.imagecount)+".jpg"
            DispatchQueue.main.async {
                
                let img = ImagePickerManager().textToImage(drawText: "\(dateString)", inImage: imageview, atPoint: CGPoint(x: 20, y: 20))
                

                let temp =
                    Insmodel(id: 0,
                             name:  "",
                             path: "",
                             image: img)
                self.imagepathlist.append(temp)
                self.imagelist.append(img)
                self.loadbeforelist()
                self.imagecount += 1
               }
        
            
            
        }
    }
    
    @IBAction func checkinClick(_ sender: Any) {
        if checkintime.isEmpty &&
            Generalinfo.checkstatus == "incomplete"
        {
            if (imagepathlist.count >= 3) {
                checkforcheckin = true
                Lb_timer1.isHidden = false
                Lb_timer.isHidden = true
                if(setfirsttime){
                    Lb_timer1.text = Lb_timer.text!
                    setfirsttime = false
                }
                self.Bt_checkin.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
                Bt_checkin.setTitle(Localization("Contract_CheckIn"), for: .normal)
                
           
                
            } else{
                showWarningToast(title: Localization("Contract_Required"), message: Localization("Contract_MinUpload"))
            }
        }
        
    }
    
    @IBAction func saveClick(_ sender: Any) {
        submitdetail()
    }
    
    
    
    func apiResponse(response: JSON, requestCode: Int) {
        if requestCode == 1 {
            print(response)
            DispatchQueue.main.async { [self] in
                let workorderobj = response.dictionaryValue
                self.workorderobj1 = response.dictionaryValue
                let imagearr = workorderobj["propertyImage"]?.array
               
                if let str = workorderobj["timeIn"]?.int {
                    self.checkintime = String(str)
                }else{
                    self.checkintime = ""
                }
       

                self.techcomment = workorderobj["techComment"]?.string ?? ""
                self.checktimeout = workorderobj["timeOut"]?.string ?? ""
             
                if let str = workorderobj["totalCrewMember"]?.int {
                    self.totalCrewMember = String(str)
                }
                if let str = workorderobj["status"]?.string?.lowercased() {
                    Generalinfo.self.checkstatus = str
                }
                if (!self.status.elementsEqual("incomplete")) {
                
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    let numberIN: Int64? = Int64(self.checkintime)
                    let workdate = dateFormatter.string(from: Date(milliseconds: numberIN ?? 0))
                    print(workdate)
                    self.Lb_date.text = workdate
                    dateFormatter.dateFormat = "HH:mm:ss"
                    let numberINtime: Int64? = Int64(self.checkintime)
                    let worktime = dateFormatter.string(from: Date(milliseconds: numberINtime ?? 0))
                    print(workdate)
                    self.Lb_timer1.text = worktime
                    

                    self.checkforcheckin = true
                    
                }
                self.Lb_propertyname.text = workorderobj["propertyName"]?.string
                for i in imagearr!{
                    if let str = i["imageData"].string {
                        let url = URL(string: str)
                        print("url = ",url)
                        self.Img_property.kf.setImage(with: url)
                    }
                }
       
                self.Lb_name.text = workorderobj["propertyManagerName"]?.string
                self.Lb_contact.text = "\(Localization("Contract_Contact")) - "+(workorderobj["propertyManagerCellNo"]?.string)!
                self.Lb_email.text = "\(Localization("Contract_Email")) - "+(workorderobj["propertyEmailid"]?.string)!
                self.Lb_address.text = workorderobj["propertyAddress"]?.string
                self.Lb_workorder.text = "\(Localization("Contract_Workorder")) :     "+self.workorderid
                self.Lb_workorderdate.text = "\(Localization("Contract_WorkDate")) :     "+(workorderobj["workOrderDate"]?.string)!
                self.Lb_technician.text = "\(Localization("Contract_Technician")) :     "+(workorderobj["crewFirstName"]?.string!)!+" "+(workorderobj["crewLastName"]?.string!)!
                self.Lb_comment.text = workorderobj["workOrderComment"]?.string
                self.Lb_count.text = self.totalCrewMember
                var taskarr = workorderobj["workOrderTaskList"]?.array
                self.tasklist.removeAll()
                var index = 0
                if(taskarr!.count > 0){
                    for j in taskarr!{
                        print("tasklist tag = ",index)
                        var taskname = "" , instruction = "", status = "",taskid = ""
                        if let str = j["taskId"].int{
                            taskid = String(str)
                        }
                        if let str = j["taskName"].string{
                            taskname = str
                        }
                        if let str = j["serviceInstruction"].string{
                            instruction = str
                        }
                        if let str = j["status"].string{
                            status = str
                        }
                        var temp =
                            Taskmodel(tastid: taskid,
                                      taskName:  taskname,
                                      serviceInstruction: instruction,
                                      status:  status,
                                      checkforarrow: false,
                                      status1: status
                        )
                        
                        self.tasklist.append(temp)
                        
                    }
                }
                self.loadtasklist()
                var berforeimagearr = workorderobj["beforeImage"]?.array
                if (berforeimagearr!.count > 0) {
                    self.imagepathlist.removeAll()
                    self.imagelist.removeAll()
                    for k in berforeimagearr!{
                        
                        do{
                            let data = try Data.init(contentsOf: URL.init(string:  k["imageData"].string!)!)
                            let image: UIImage = UIImage(data: data)!
                            var temp =
                                Insmodel(id: k["id"].int!,
                                         name:  k["name"].string!,
                                         path: k["imageData"].string!,
                                         image:image
                            )
                            self.imagepathlist.append(temp)
                            self.imagelist.append(image)
                        }catch{
                            
                        }
                        
                        
                        
                    }
                    self.loadbeforelist()
                }
                
                
                
                
                
                
            }
        }
        
        if requestCode == 2 {
            print(response)
            DispatchQueue.main.async {
                var workorderobj = response.dictionaryValue
                var concat = "\(self.Lb_date.text ?? "")\("T")\(self.Lb_timer1.text ?? "")"
                self.checkintime = concat
                let refreshAlert = UIAlertController(title: Localization("Contract_Success"), message: workorderobj["message"]?.string, preferredStyle: UIAlertController.Style.alert)
                
                refreshAlert.addAction(UIAlertAction(title: Localization("Contract_Ok"), style: .default, handler: { (_: UIAlertAction!) in
                    refreshAlert.dismiss(animated: true, completion: nil)
                    let inprogressVc = self.storyboard?.instantiateViewController(withIdentifier: "inprogress") as! Inprogress
                    inprogressVc.modalPresentationStyle = .fullScreen
                    inprogressVc.getstatus = Generalinfo.self.checkstatus
                    inprogressVc.workorderid = self.workorderid
                    self.present(inprogressVc, animated: true, completion: nil)
                }))
                
                self.present(refreshAlert, animated: true, completion: nil)
            }
        }
        
        
    }
    
    @objc func hideshowtasklist(sender: UIButton!) {
        var tag: Int = sender.tag
        print("tag is = ",tag)
        
        //if sender.currentImage == UIImage(named: "downarrow") {
        if(sender.title(for: .normal) == "expand"){
        print("signale = ","downarrow")
            tasklist[tag].checkforarrow = true
        }else{
            print("signale = ","uparrow")
            tasklist[tag].checkforarrow = false
        }
        loadtasklist()
    }
    
    @objc func deletebeforeimage(sender: UIButton!) {
        var tag: Int = sender.tag
        print("tag is = ",tag)
        let refreshAlert = UIAlertController(title: Localization("Contract_Delete"), message: Localization("Contract_DeleteAlert"), preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: Localization("Contract_Cancel"), style: .default, handler: { (_: UIAlertAction!) in
            refreshAlert.dismiss(animated: true, completion: nil)
        }))
        
        refreshAlert.addAction(UIAlertAction(title: Localization("Contract_Ok"), style: .default, handler: { (_: UIAlertAction!) in
            self.imagelist.remove(at: tag)
            self.imagepathlist.remove(at: tag)
            self.loadbeforelist()
            refreshAlert.dismiss(animated: true, completion: nil)
        }))
        
        
        
        
        present(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    @objc func zoomimage(sender: UIButton!) {
        var tag: Int = sender.tag
        print("tag is = ",tag)
        DispatchQueue.global(qos: .background).async {
            do
            {
                let attributes = RappleActivityIndicatorView.attribute(style: RappleStyle.circle, tintColor: .white, progreeBarFill: .white, thickness: 7)
                RappleActivityIndicatorView.startAnimatingWithLabel(Localization("Contract_Loading"), attributes:attributes)
                
                
                DispatchQueue.main.async {
                    ANZSingleImageViewer.showImage(self.imagepathlist[tag].image, toParent: self)
                   RappleActivityIndicatorView.stopAnimation()
                }
            }
            catch {
                // error
            }
        }
    }
    
    
    func loadbeforelist(){
        for v in self.stack_before.subviews {
            v.removeFromSuperview()
        }
        for(index,element) in self.imagepathlist.enumerated(){
            let tempView1 = Bundle.main.loadNibNamed("Beforepicture", owner: self, options: nil)!.first as? Beforepicture
            tempView1?.layoutIfNeeded()
            tempView1?.layoutSubviews()
            tempView1?.beforeimage.tag = index
            tempView1?.deleteBtn.tag = index
            tempView1?.imageBtn.tag = index
            if(checkforcheckin){
                tempView1?.deleteBtn.isHidden = true
            }else{
                tempView1?.deleteBtn.isHidden = false
            }
            tempView1?.deleteBtn.addTarget(self, action: #selector(self.deletebeforeimage), for: UIControl.Event.touchUpInside)
            tempView1?.imageBtn.addTarget(self, action: #selector(self.zoomimage(sender:)), for: UIControl.Event.touchUpInside)
            tempView1?.widthAnchor.constraint(equalToConstant: (tempView1?.mainView.frame.width)!+10).isActive = true
            //tempView1?.beforeimage.contentMode = .scaleAspectFit
            // do{
            //   let data = try Data.init(contentsOf: URL.init(string: element.path)!)
            //  let image: UIImage = UIImage(data: data)!
            tempView1?.beforeimage.image = element.image
            //}catch{
            
            //}
            tempView1?.layoutIfNeeded()
            self.stack_before.addArrangedSubview(tempView1!)
            self.stack_before.layoutSubviews()
        }
    }
    
    
    
    func loadtasklist(){
        for v in stack_tasklist.subviews {
            v.removeFromSuperview()
        }
        for(index,element) in tasklist.enumerated(){
            let tempView1 = Bundle.main.loadNibNamed("Tasklist", owner: self, options: nil)!.first as? Tasklist
            tempView1?.layoutIfNeeded()
            tempView1?.layoutSubviews()
            tempView1?.Lb_task.text = element.taskName
            tempView1?.Lb_instruct.text = element.serviceInstruction
            tempView1?.Bt_updown.tag = index
            tempView1?.Lb_task.tag = index
            tempView1?.Lb_instruct.tag = index
            tempView1?.Bt_expand.tag = index
            print("checkforarrow = ",element.checkforarrow)
            if(element.checkforarrow){
               tempView1?.Bt_expand.setTitle("collapse", for: .normal)
                tempView1?.Bt_updown.setImage(UIImage(named: "uparrow"), for: .normal)
                tempView1?.Lb_instruct.isHidden = false
                // tempView1?.heightAnchor.constraint(greaterThanOrEqualToConstant: (tempView1?.taskview.frame.height)!+10).isActive = true
            }else{
                 tempView1?.Bt_expand.setTitle("expand", for: .normal)
                tempView1?.Bt_updown.setImage(UIImage(named: "downarrow"), for: .normal)
                tempView1?.Lb_instruct.isHidden = true
             tempView1?.heightAnchor.constraint(greaterThanOrEqualToConstant: 0).isActive = true
            }
            
            tempView1?.Bt_expand.addTarget(self, action: #selector(self.hideshowtasklist), for: UIControl.Event.touchUpInside)
           //tempView1?.heightAnchor.constraint(equalToConstant: (tempView1?.taskview.frame.height)!+10).isActive = true
            tempView1?.heightAnchor.constraint(greaterThanOrEqualToConstant: 0).isActive = true
            tempView1?.layoutIfNeeded()
            self.stack_tasklist.addArrangedSubview(tempView1!)
            self.stack_tasklist.layoutSubviews()
            self.stack_tasklist.autoresizesSubviews = true
            
        }
    }
    
    
    func submitdetail() {
        if checkintime.isEmpty &&
            Generalinfo.checkstatus == "incomplete"
        {
            if(checkforcheckin){
                if (imagepathlist.count >= 3) {
                    myImageUploadRequest()
                } else{
                    showWarningToast(title: Localization("Contract_Required"), message: Localization("Contract_MinUpload"))
                }
                
            }else{
                showWarningToast(title: Localization("Contract_Required"), message: Localization("Contract_checkinRequired"))
            }
        }else{
            let inprogressVc = self.storyboard?.instantiateViewController(withIdentifier: "inprogress") as! Inprogress
            inprogressVc.modalPresentationStyle = .fullScreen
            inprogressVc.getstatus = Generalinfo.self.checkstatus
            inprogressVc.workorderid = self.workorderid
            self.present(inprogressVc, animated: true, completion: nil)
        }
    }
    
    func workorderupdate(time :String){
        let url = "\(self.BASE_URL)workorderupdate"
        print("url = ",url)
        self.workorderobj1.updateValue("inprogress", forKey: "status")
        
 
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from:time)!
        let timeinMiliSecond = date.millisecondsSince1970 // 1476889390939

        self.workorderobj1.updateValue(timeinMiliSecond, forKey: "timeIn")

        self.workorderobj1.updateValue(checktimeout, forKey: "timeOut")
        self.workorderobj1.updateValue(totalCrewMember, forKey: "totalCrewMember")
        self.workorderobj1.updateValue(techcomment, forKey: "techComment")

        self.workorderobj1["timeInLatitude"] = "\(self.strLat)"

        self.workorderobj1["timeInLongitude"] = "\(self.strLong)"

        
         
        
        var jsonArray = [Any]()
        for (index,element)in imagepathlist.enumerated() {
            var jsonObject = [String:String]()
            print("berfore image name = ", element.name)
            var id = String(element.id)
            if !id.isEmpty{
                id = ""
            }
            jsonObject = ["id":id,"name":element.name]
            jsonArray.append(jsonObject)
        }
        self.workorderobj1.updateValue(jsonArray, forKey: "beforeImage")
        putRequest(apiDelegate: self, requestCode: 2, url: url, params: workorderobj1)
    }
    
    
    func myImageUploadRequest() {
        let parameters = ["": ""]
        DispatchQueue.main.async {
            let attributes = RappleActivityIndicatorView.attribute(style: RappleStyle.circle, tintColor: .white, progreeBarFill: .white, thickness: 5)
            RappleActivityIndicatorView.startAnimatingWithLabel(Localization("Contract_Uploading"), attributes: attributes)
            
        }
        
        let token: String = "Bearer "+UserDefaults.standard.string(forKey: "accessToken")!
        // Image to upload:
        // Server address (replace this with the address of your own server):
        print("yes")
        let url = "\(BASE_URL)upload/beforeImage"
        //let data = defectImage.image!.pngData()
        
        
        print(url)
        // Use Alamofire to upload the image
        DispatchQueue.main.async {
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": token
            ]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                //if let data = data {
                for(index,element) in self.imagelist.enumerated(){
                    let image = element as! UIImage
                    let thumbImage = image.wxCompress()
                   // let data = thumbImage.jpegData(compressionQuality: 0.5)
                    let data = thumbImage.jpegData(compressionQuality: 0.5)
                    var imagename = self.getcurrentTime()
                    var filename = "\(imagename)\(String(index))\(".jpg")"
                    print("filename = ",filename)
                    self.imagepathlist[index].name = filename
                    multipartFormData.append(data!, withName: "beforeImage", fileName: filename, mimeType: "image/jpg")
                }
                //}
                
            }, usingThreshold: UInt64(), to: url, method: .post, headers: headers) { result in
                
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        do {
                            DispatchQueue.main.async {
                                RappleActivityIndicatorView.stopAnimation()
                            }
                            let json = try JSON(data: response.data!)
                            print("image upload = ",json)
                            var concat = "\(self.Lb_date.text ?? "")\("T")\(self.Lb_timer1.text ?? "")"
                            print("concat = ",concat)
                            self.workorderupdate(time: concat)
                            
                        } catch {
                            DispatchQueue.main.async {
                                RappleActivityIndicatorView.stopAnimation()
                            }
                            self.showCommanAlert(title: "Error", message: "\(error)")
                        }
                    }
                case let .failure(error):
                    DispatchQueue.main.async {
                        RappleActivityIndicatorView.stopAnimation()
                        self.showCommanAlert(title: "Error", message: "Error in upload: \(error.localizedDescription)")
                    }
                    
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor =  #colorLiteral(red: 0.0943563953, green: 0.2551338077, blue: 0.2927393913, alpha: 1)
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
        return .lightContent
    }

}


extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }

    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}


//MARK:
//MARK: -----------Localisation Function

extension Generalinfo{
    
    func configureViewFromLocalisation() {
        lbl_Title.text =  Localization("Contract_GeneralInfo")
        lbl_S_WorkOrderInfo.text =  Localization("Contract_WorkOrderInfo")
        lbl_S_WorkOrderComment.text =  Localization("Contract_WorkOrderComment")
        lbl_S_CrewMember.text =  Localization("Contract_CrewMembers")
        lbl_S_TaskList.text =  Localization("Contract_TaskList")

        lbl_S_BeforeImage.text =  Localization("Contract_BeforeImage")
        lbl_S_PleaseUploadMin.text =  Localization("Contract_MinUpload")

        Bt_checkin.setTitle(Localization("Contract_CheckIn"), for: .normal)
        Bt_save.setTitle(Localization("Contract_Save"), for: .normal)
 
    }
}
