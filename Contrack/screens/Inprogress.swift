//
//  Inprogress.swift
//  Contrack
//
//  Created by Vikram Singh on 07/11/20.
//  Copyright © 2020 Vikram Singh. All rights reserved.
//

import UIKit
import SwiftyJSON
import RappleProgressHUD
import ANZSingleImageViewer
import Alamofire

class Inprogress: Common,APIDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate{
    
    
    @IBOutlet weak var lbl_S_Title: UILabel!
    @IBOutlet weak var lbl_S_TaskList: UILabel!
    @IBOutlet weak var lbl_S_CompleteTaskChecked: UILabel!
    @IBOutlet weak var lbl_S_Comment: UILabel!
    @IBOutlet weak var lbl_S_AfterPicture: UILabel!
    @IBOutlet weak var lbl_S_UploadMin: UILabel!

    
    @IBOutlet weak var cardwidth: NSLayoutConstraint!
    @IBOutlet weak var Bt_checkout: UIButton!
    @IBOutlet weak var Bt_save: UIButton!
    @IBOutlet weak var stack_afterpicture: UIStackView!
    @IBOutlet weak var addphotocardview: CardView!
    @IBOutlet weak var Bt_addphoto: UIButton!
    @IBOutlet weak var Tf_comment: UITextField!
    @IBOutlet weak var stack_tasklist: UIStackView!
    var workorderid = "", getstatus = "",techcomment = "",commentvalue = ""
    var workorderobj1 = [String:Any]()
    var checkforout = false,checkforimage = false
    var tasklist = [Taskmodel]()
    var imagepathlist = [Insmodel]()
    var imagelist = [Any]()
    var imagecount = 0
 
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewFromLocalisation()
        Tf_comment.delegate = self
        if getstatus == "complete" || getstatus == "partially_complete"{
            Bt_save.isHidden = true
            Bt_checkout.setTitle("Next", for: .normal)
            addphotocardview.isHidden = true
            cardwidth.constant = 0
        }
        DispatchQueue.main.async {
            self.showindicator()
            let url = "\(self.BASE_URL)workorder?workOrderId="+self.workorderid
            print("url = ",url)
            self.getRequest(apiDelegate: self, requestCode: 1, url: url, params: [:], isIndicatorShow: false)
        }
        
      
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.first
            let topPadding = window?.safeAreaInsets.top
            let statusBar = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: topPadding ?? 0.0))
            statusBar.backgroundColor = Inprogress.hexStringToUIColor(hex: appThemeColor)
            UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.addSubview(statusBar)
        }
    }
 
    
     func SettingView() {
         let alertController = UIAlertController (title: Localization("Contract_LocationDisabled"), message: Localization("Contract_LocationMessage"), preferredStyle: .alert)

         let settingsAction = UIAlertAction(title: Localization("Contract_Settings"), style: .default) { (_) -> Void in

            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }

            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
     

        present(alertController, animated: true, completion: nil)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
      //  locationManager.stopUpdatingLocation()
    }
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
              self.Tf_comment.becomeFirstResponder()
               self.view.endEditing(true)
              return true
          }
    
    
    func imagePickerController(_: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            
        }
    }
    
    func imagePickerControllerDidCancel(picker _: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func backClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func addphotoClick(_ sender: Any) {
        ImagePickerManager().pickImage(self) { imageview in
            // here is the image
            //self.profleImage.image = imageview
            
            //var filename = "image"+String(self.imagecount)+".jpg"
            let date = Date()
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd hh:mm:ss"
            let dateString = df.string(from: date)
            DispatchQueue.main.async {
                let img = ImagePickerManager().textToImage(drawText: "\(dateString)", inImage: imageview, atPoint: CGPoint(x: 20, y: 20))

                let temp =
                    Insmodel(id: 0,
                             name:  "",
                             path: "",
                             image: img)
                self.imagepathlist.append(temp)
                self.imagelist.append(img)
                self.loadafterlist()
                self.imagecount += 1
                self.checkforimage = true
                self.checkforout = false
               }
        
        }
    }
    
    @IBAction func saveClick(_ sender: Any) {
        commentvalue = Tf_comment.text!
        if (commentvalue.isEmpty){
            showWarningToast(title: Localization("Contract_Required"), message: Localization("Contract_CommentReq"))
            
        }else {
            if (imagelist.count >= 3) {
                if (checkforimage){
                    myImageUploadRequest(comment: commentvalue)
                }else {
                    workorderupdate(comment: commentvalue)
                }
            } else{
                showWarningToast(title: Localization("Contract_Required"), message: Localization("Contract_MinUpload") + "*")
            }
        }
        
    }
    
    
    @IBAction func checkoutClick(_ sender: Any) {
         self.checkforout = true
          for(index,element) in tasklist.enumerated(){
            var status = element.status
            var status1 = element.status1
            if(status1 != status){
                checkforout = false
            }
        }
        
        if(!getstatus.elementsEqual("complete") && !getstatus.elementsEqual("partially_complete")) {
            commentvalue = Tf_comment.text!
            print("techcomment = ",techcomment)
            print("comment = ",commentvalue)
            if (commentvalue.isEmpty){
                showWarningToast(title: Localization("Contract_Required"), message: Localization("Contract_CommentReq"))
            }else if (imagepathlist.count < 3) {
                showWarningToast(title: Localization("Contract_Required"), message: Localization("Contract_beforeImg"))
            } else if (!checkforout || !techcomment.elementsEqual(commentvalue)) {
                if (!techcomment.elementsEqual(commentvalue)){
                    checkforout = false
                }
             
                showdialog(msg:Localization("Contract_Alert"),body:Localization("Contract_SaveAlert"))
            } else {
                
                
                let strOutDate = getCurrentDate(format: "yyyy-MM-dd")
                let strOutTime = getCurrentDate(format: "HH:mm:ss")
                let concat = "\(strOutDate)\("T")\(strOutTime)"
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                let date = dateFormatter.date(from:concat)!
                let timeOutMiliSecond = date.millisecondsSince1970 // 1476889390939
                self.workorderobj1.updateValue(timeOutMiliSecond, forKey: "timeOut")

                let reviewVc = self.storyboard?.instantiateViewController(withIdentifier: "revieworder") as! Revieworder
                reviewVc.modalPresentationStyle = .fullScreen
                reviewVc.workorderid = workorderid
                reviewVc.workorderobj1 = self.workorderobj1
                self.present(reviewVc, animated: true, completion: nil)
                
            }
        }else{
            let reviewVc = self.storyboard?.instantiateViewController(withIdentifier: "revieworder") as! Revieworder
            reviewVc.modalPresentationStyle = .fullScreen
              reviewVc.workorderid = workorderid
            reviewVc.workorderobj1 = self.workorderobj1

            self.present(reviewVc, animated: true, completion: nil)
            
        }
        
    }
    
    func myImageUploadRequest(comment: String) {
        let parameters = ["": ""]
        DispatchQueue.main.async {
            let attributes = RappleActivityIndicatorView.attribute(style: RappleStyle.circle, tintColor: .white, progreeBarFill: .white, thickness: 5)
            RappleActivityIndicatorView.startAnimatingWithLabel(Localization("Contract_Uploading"), attributes: attributes)
            
        }
        
        let token: String = "Bearer "+UserDefaults.standard.string(forKey: "accessToken")!
        // Image to upload:
        // Server address (replace this with the address of your own server):
        print("yes")
        let url = "\(BASE_URL)upload/afterImage"
        //let data = defectImage.image!.pngData()
        
        
        print(url)
        // Use Alamofire to upload the image
        DispatchQueue.main.async {
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": token
            ]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                //if let data = data {
                for(index,element) in self.imagelist.enumerated(){
                    let image = element as! UIImage
                    let thumbImage = image.wxCompress()
                    //let data = thumbImage.jpegData(compressionQuality: 0)
                  //  let data = thumbImage.pngData()
                    let data = thumbImage.jpegData(compressionQuality: 0.5)

                    var imagename = self.getcurrentTime()
                    var filename = "\(imagename)\(String(index))\(".jpg")"
                    print("filename = ",filename)
                    self.imagepathlist[index].name = filename
                    multipartFormData.append(data!, withName: "afterImage", fileName: filename, mimeType: "image/jpg")
                }
                //}
                
            }, usingThreshold: UInt64(), to: url, method: .post, headers: headers) { result in
                
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        do {
                            DispatchQueue.main.async {
                                RappleActivityIndicatorView.stopAnimation()
                            }
                            let json = try JSON(data: response.data!)
                            print("image upload = ",json)
                            self.workorderupdate(comment: comment)
                            
                        } catch {
                            DispatchQueue.main.async {
                                RappleActivityIndicatorView.stopAnimation()
                            }
                            self.showCommanAlert(title: "Error", message: "\(error)")
                        }
                    }
                case let .failure(error):
                    DispatchQueue.main.async {
                        RappleActivityIndicatorView.stopAnimation()
                    }
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    
    func workorderupdate(comment:String){
        let url = "\(self.BASE_URL)workorderupdate"
        print("url = ",url)
        self.workorderobj1.updateValue(comment, forKey: "techComment")
        

//        
        var jsonArray = [Any]()
        var taskarr = [Any]()
        for (index,element)in imagepathlist.enumerated() {
            var jsonObject = [String:String]()
            print("after image name = ", element.name)
            var id = String(element.id)
            if id.isEmpty || id.elementsEqual("0") {
                id = ""
            }
            jsonObject = ["id":id,"name":element.name]
            jsonArray.append(jsonObject)
        }
        self.workorderobj1.updateValue(jsonArray, forKey: "afterImage")
        
        for(index,element)in tasklist.enumerated(){
            var jsonObject = [String:String]()
            jsonObject = ["taskId":element.tastid,"taskName":element.taskName,"serviceInstruction":element.serviceInstruction,"status":element.status]
            taskarr.append(jsonObject)
        }
        self.workorderobj1.updateValue(taskarr, forKey: "workOrderTaskList")
        putRequest(apiDelegate: self, requestCode: 2, url: url, params: workorderobj1)
    }
    
    
    
    func apiResponse(response: JSON, requestCode: Int) {
        if requestCode == 1 {
            print(response)
            DispatchQueue.main.async {
                var workorderobj = response.dictionaryValue
                self.workorderobj1 = response.dictionaryValue
                self.techcomment = workorderobj["techComment"]?.string ?? ""
                if(!self.techcomment.isEmpty){
                    self.checkforout = true
                }
                self.Tf_comment.text = self.techcomment
                
                var taskarr = workorderobj["workOrderTaskList"]?.array
                self.tasklist.removeAll()
                var index = 0
                if(taskarr!.count > 0){
                    for j in taskarr!{
                        print("tasklist tag = ",index)
                        var taskname = "" , instruction = "", status = "",taskid = ""
                        if let str = j["taskId"].int{
                            taskid = String(str)
                        }
                        if let str = j["taskName"].string{
                            taskname = str
                        }
                        if let str = j["serviceInstruction"].string{
                            instruction = str
                        }
                        if let str = j["status"].string{
                            status = str
                        }
                        var temp =
                            Taskmodel(tastid: taskid,
                                      taskName:  taskname,
                                      serviceInstruction: instruction,
                                      status:  status,
                                      checkforarrow: false,
                                      status1: status
                        )
                        
                        self.tasklist.append(temp)
                        
                    }
                }
                self.loadtasklist()
                var berforeimagearr = workorderobj["afterImage"]?.array
                if (berforeimagearr!.count > 0) {
                    self.imagepathlist.removeAll()
                    self.imagelist.removeAll()
                    for k in berforeimagearr!{
                        
                        do{
                            let data = try Data.init(contentsOf: URL.init(string:  k["imageData"].string!)!)
                            let image: UIImage = UIImage(data: data)!
                            var temp =
                                Insmodel(id: k["id"].int!,
                                         name:  k["name"].string!,
                                         path: k["imageData"].string!,
                                         image:image
                            )
                            self.imagepathlist.append(temp)
                            self.imagelist.append(image)
                        }catch{
                            
                        }
                        
                        
                        
                    }
                    self.loadafterlist()
                }else{
                    DispatchQueue.main.async {
                        RappleActivityIndicatorView.stopAnimation()
                    }
                }
                
                
                
                
                
                
            }
        }
        
        if requestCode == 2 {
            print(response)
            DispatchQueue.main.async {
                var workorderobj = response.dictionaryValue
                self.checkforout = true
                self.checkforimage = false
                self.techcomment = self.commentvalue
                Generalinfo.checkstatus = "inprogress"
                for(index,element) in self.tasklist.enumerated(){
                           var status = element.status
                    self.tasklist[index].status1 = status
                       }
                
                let refreshAlert = UIAlertController(title: Localization("Contract_Success"), message: workorderobj["message"]?.string, preferredStyle: UIAlertController.Style.alert)
                
                refreshAlert.addAction(UIAlertAction(title: Localization("Contract_Ok"), style: .default, handler: { (_: UIAlertAction!) in
                    refreshAlert.dismiss(animated: true, completion: nil)
                }))
                
                self.present(refreshAlert, animated: true, completion: nil)
                
            }
        }
        
    }
    
    
    
    
    
    
    func loadafterlist(){
        for v in self.stack_afterpicture.subviews {
            v.removeFromSuperview()
        }
        for(index,element) in self.imagepathlist.enumerated(){
            let tempView1 = Bundle.main.loadNibNamed("Beforepicture", owner: self, options: nil)!.first as? Beforepicture
            tempView1?.layoutIfNeeded()
            tempView1?.layoutSubviews()
            tempView1?.beforeimage.tag = index
            tempView1?.deleteBtn.tag = index
            tempView1?.imageBtn.tag = index
            if(getstatus == "inprogress" || getstatus == "incomplete") {
                tempView1?.deleteBtn.isHidden = false
            }else{
                tempView1?.deleteBtn.isHidden = true
            }
            tempView1?.deleteBtn.addTarget(self, action: #selector(self.deletebeforeimage), for: UIControl.Event.touchUpInside)
            tempView1?.imageBtn.addTarget(self, action: #selector(self.zoomimage(sender:)), for: UIControl.Event.touchUpInside)
            tempView1?.widthAnchor.constraint(equalToConstant: (tempView1?.mainView.frame.width)!+10).isActive = true
            //tempView1?.beforeimage.contentMode = .scaleAspectFit
            // do{
            //   let data = try Data.init(contentsOf: URL.init(string: element.path)!)
            //  let image: UIImage = UIImage(data: data)!
            tempView1?.beforeimage.image = element.image
            //}catch{
            
            //}
            tempView1?.layoutIfNeeded()
            self.stack_afterpicture.addArrangedSubview(tempView1!)
            self.stack_afterpicture.layoutSubviews()
        }
        DispatchQueue.main.async {
            RappleActivityIndicatorView.stopAnimation()
        }
        
    }
    
    
    
    @objc func deletebeforeimage(sender: UIButton!) {
        var tag: Int = sender.tag
        print("tag is = ",tag)
        let refreshAlert = UIAlertController(title: Localization("Contract_Delete"), message: Localization("Contract_DeleteAlert"), preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: Localization("Contract_Cancel"), style: .default, handler: { (_: UIAlertAction!) in
            refreshAlert.dismiss(animated: true, completion: nil)
        }))
        
        refreshAlert.addAction(UIAlertAction(title: Localization("Contract_Ok"), style: .default, handler: { (_: UIAlertAction!) in
            self.imagelist.remove(at: tag)
            self.imagepathlist.remove(at: tag)
            self.loadafterlist()
            if(self.imagelist.count == 0){
                self.checkforimage = false
            }
            refreshAlert.dismiss(animated: true, completion: nil)
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    @objc func zoomimage(sender: UIButton!) {
        var tag: Int = sender.tag
        print("tag is = ",tag)
        DispatchQueue.global(qos: .background).async {
            do
            {
                let attributes = RappleActivityIndicatorView.attribute(style: RappleStyle.circle, tintColor: .white, progreeBarFill: .white, thickness: 7)
                RappleActivityIndicatorView.startAnimatingWithLabel(Localization("Contract_Loading"), attributes:attributes)
                
                
                DispatchQueue.main.async {
                    ANZSingleImageViewer.showImage(self.imagepathlist[tag].image, toParent: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            catch {
                // error
            }
        }
    }
    
    
    
    
    
    
    func loadtasklist(){
        for v in stack_tasklist.subviews {
            v.removeFromSuperview()
        }
        for(index,element) in tasklist.enumerated(){
            let tempView1 = Bundle.main.loadNibNamed("Inprogresstasklist", owner: self, options: nil)!.first as? Inprogresstasklist
            tempView1?.layoutIfNeeded()
            tempView1?.layoutSubviews()
            
            tempView1?.Chkbox_task.tag = index
            tempView1?.Lb_checkbox.text = element.taskName
            tempView1?.Bt_inprogress.tag = index
            var status = element.status.lowercased()
           if (status == "complete") {
                tempView1?.Chkbox_task.isSelected = true
            }else{
                tempView1?.Chkbox_task.isSelected = false
            }
            
          
            
            tempView1?.Bt_inprogress.addTarget(self, action: #selector(self.checkboxclick), for: UIControl.Event.touchUpInside)
            
           
            
            tempView1?.heightAnchor.constraint(equalToConstant: (tempView1?.stack_holder.frame.height)!+10).isActive = true
            tempView1?.layoutIfNeeded()
            self.stack_tasklist.addArrangedSubview(tempView1!)
            self.stack_tasklist.layoutSubviews()
            
        }
    }
    
    @objc func checkboxclick(sender: UIButton!) {
        var tag: Int = sender.tag
        print("tag is = ",tag)
        var status = tasklist[tag].status.lowercased()
        print("status = ",status)
      
        if(status == "complete"){
            print("checked  = ", "false")
            tasklist[tag].status = "INCOMPLETE"
            
        }else{
            print("checked  = ", "false")
            tasklist[tag].status = "COMPLETE"
        }
        
        
//        if (!status.elementsEqual("complete")) {
//            if (sender.isSelected) {
//                print("checked  = ", "true")
//                tasklist[tag].status = "COMPLETE"
//                checkforout = false
//            } else {
//                print("checked  = ", "false")
//                tasklist[tag].status = "INCOMPLETE"
//                checkforout = true
//            }
//        } else{
//             if(sender.isSelected){
//                print("checked  = ", "false")
//                tasklist[tag].status = "INCOMPLETE"
//                checkforout = true
//                sender.isSelected = false
//            }
//
//        }
        
        loadtasklist()
    }
    
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor =  #colorLiteral(red: 0.0943563953, green: 0.2551338077, blue: 0.2927393913, alpha: 1)
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
        return .lightContent
    }
}



//MARK:
//MARK: -----------Localisation Function

extension Inprogress{
    
    func configureViewFromLocalisation() {
        lbl_S_Title.text =  Localization("Contract_InProgress")
        lbl_S_TaskList.text =  Localization("Contract_TaskList")
        lbl_S_CompleteTaskChecked.text =  Localization("Contract_CompleteTaskChecked")
        lbl_S_Comment.text =  Localization("Contract_Comments") + "*"
        lbl_S_AfterPicture.text =  Localization("Contract_AfterPicture")
        lbl_S_UploadMin.text =  Localization("Contract_MinUpload")

        Bt_save.setTitle(Localization("Contract_Save"), for: .normal)
        Bt_checkout.setTitle(Localization("Contract_CheckOut"), for: .normal)


        
    }
}
