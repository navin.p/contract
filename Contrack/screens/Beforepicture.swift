//
//  Beforepicture.swift
//  Contrack
//
//  Created by Vikram Singh on 04/11/20.
//  Copyright © 2020 Vikram Singh. All rights reserved.
//

import UIKit

class Beforepicture: UIView {

    @IBOutlet weak var mainView: CardView!
    @IBOutlet weak var beforeimage: UIImageView!
    @IBOutlet weak var imageBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
}
