//
//  Login.swift
//  Contrack
//
//  Created by Vikram Singh on 14/10/20.
//  Copyright © 2020 Vikram Singh. All rights reserved.
//

import UIKit
import SwiftyJSON
import DLRadioButton
class Login: Common, APIDelegate{
    
    @IBOutlet weak var Bt_hideshow: UIButton!
    @IBOutlet weak var Rb_remember: DLRadioButton!
    @IBOutlet weak var login_bt: UIButton!
    @IBOutlet weak var textbox_password: UITextField!
    @IBOutlet weak var Textbox_email: UITextField!
    
    @IBOutlet weak var lbl_Login: UILabel!
    @IBOutlet weak var lbl_RememberMe: UILabel!
    @IBOutlet weak var lbl_ForgotPass: UILabel!
    @IBOutlet weak var img_Logo: UIImageView!

    var usernamevalue = "",passwordvalue = ""
    var checkforlogin = false , remembercheck = false,iconClick = true
    var defaults = UserDefaults.standard
    var count = 0
   
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewFromLocalisation()
        modalPresentationCapturesStatusBarAppearance = true
        Textbox_email.delegate = self
        textbox_password.delegate = self
//        Textbox_email.text = "crewtech"
//        textbox_password.text = "testmeout"
        NotificationCenter.default.addObserver(self, selector: #selector(dismisslogin), name: NSNotification.Name(rawValue: "dismisslogin"), object: nil)
        //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismisssplash"), object: nil)
        Rb_remember.addTarget(self, action: #selector(radioclick), for: UIControl.Event.touchUpInside)
        
        
        if(self.defaults.value(forKey: "email") != nil){
            let email = UserDefaults.standard.value(forKey: "email")
            Textbox_email.text = (email  as! String)
        }
        if(self.defaults.value(forKey: "pass") != nil){
            let pass = UserDefaults.standard.value(forKey: "pass")
            textbox_password.text = (pass as! String)
        }
           
    }
            
    
    @IBAction func showhidepassclick(_ sender: Any) {
        if(iconClick == true) {
                   textbox_password.isSecureTextEntry = false
            let visibleimage = UIImage(named: "visibility")
                   Bt_hideshow.setImage(visibleimage, for: .normal)
               } else {
                   textbox_password.isSecureTextEntry = true
                   let hideimage = UIImage(named: "hide")
                   Bt_hideshow.setImage(hideimage, for: .normal)
               }
               
               iconClick = !iconClick
    }
    
    
    @objc func radioclick(sender: UIButton!) {
        print("count is = ",count)
        if(count == 0){
            print("click", "select")
            remembercheck = true
        }else{
            count = -1
            sender.isSelected = false
            print("click", "unselect")
            remembercheck = false
        }
           count += 1
        
    }
    

    
    @objc func dismisslogin() {
        dismiss(animated: true, completion: nil)
    }
    
 
    @IBAction func loginclick(_ sender: Any) {
        usernamevalue = Textbox_email.text!
        passwordvalue = textbox_password.text!
        if(UserDefaults.standard.value(forKey: "SavedLogin") != nil){
            let ary = (UserDefaults.standard.value(forKey: "SavedLogin") as! NSArray).mutableCopy() as! NSMutableArray
         
        
            if !(ary.contains("\(Textbox_email.text ?? "")")){
                ary.add("\(Textbox_email.text ?? "")")
                UserDefaults.standard.setValue(ary, forKey: "SavedLogin")
            }
            
        }else{
            let ary = NSMutableArray()
            ary.add("\(Textbox_email.text ?? "")")
            UserDefaults.standard.setValue(ary, forKey: "SavedLogin")
        }
        if validate() {
            let params = ["usernameOrEmail": usernamevalue, "password": passwordvalue]
            let url = "\(BASE_URL)signin"
            loginpost(apiDelegate: self, requestCode: 1, url: url, params: params, isIndicatorShow: true)
        }
    }
    
    @IBAction func forgotclick(_ sender: Any) {
        let loginvc = storyboard?.instantiateViewController(withIdentifier: "forgot") as! Forgotpass
        loginvc.modalPresentationStyle = .fullScreen
        present(loginvc, animated: true, completion: nil)
    }
    
    
    
    
    func validate() -> Bool {
        var valid = true
        if usernamevalue.isEmpty {
            showWarningToast(title: Localization("Contract_FieldRequired"), message: Localization("Contract_validUserName"))
            valid = false
        } else if passwordvalue.isEmpty {
            showWarningToast(title: Localization("Contract_FieldRequired"), message: Localization("Contract_validPassword"))
            valid = false
        }
        return valid
    }
    
    
    func apiResponse(response: JSON, requestCode: Int) {
        
        if requestCode == 1 {
           
           
            print(response)
            DispatchQueue.main.async { [self] in
                if(UserDefaults.standard.value(forKey: "SavedLogin") != nil){
                    let ary = (UserDefaults.standard.value(forKey: "SavedLogin") as! NSArray).mutableCopy() as! NSMutableArray
                 
                
                    if !(ary.contains("\(Textbox_email.text ?? "")")){
                        ary.add("\(Textbox_email.text ?? "")")
                        UserDefaults.standard.setValue(ary, forKey: "SavedLogin")
                    }
                    
                }else{
                    let ary = NSMutableArray()
                    ary.add("\(Textbox_email.text ?? "")")
                    UserDefaults.standard.setValue(ary, forKey: "SavedLogin")
                }
                var accesstoen = ""
                var tokentype = ""
                var crewid = ""
                var crewname = ""
                var vendorid = ""
                var vendorname = ""
                
                if let str = response["accessToken"].string {
                    accesstoen = str
                }
                if let str = response["tokenType"].string {
                    tokentype = str
                }
                if let str = response["crewId"].int {
                    crewid = String(str)
                }
                if let str = response["crewName"].string {
                    crewname = str
                }
                if let str = response["vendorName"].string {
                    vendorname = str
                }
                if let str = response["vendorId"].int {
                    vendorid = String(str)
                }
                if(self.remembercheck){
                    self.defaults.set(true, forKey: "checklogin")
                }
                self.defaults.set(accesstoen, forKey: "accessToken")
                self.defaults.set(tokentype, forKey: "tokenType")
                self.defaults.set(crewid, forKey: "crewId")
                self.defaults.set(crewname, forKey: "crewName")
                self.defaults.set(vendorname, forKey: "vendorName")
                self.defaults.set(vendorid, forKey: "vendorId")
                
                self.defaults.set(self.Textbox_email.text, forKey: "email")
                self.defaults.set(self.textbox_password.text, forKey: "pass")
                let date = Date().addingTimeInterval(TimeInterval(15.0 * 60.0))
                UserDefaults.standard.setValue(date, forKey: "ContractApp_LogOutTime")

                let loginvc = self.storyboard?.instantiateViewController(withIdentifier: "appointmentlist") as! Appointmentlist
                loginvc.modalPresentationStyle = .fullScreen
                self.present(loginvc, animated: true, completion: nil)
                
                
            }
        }
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor =  #colorLiteral(red: 0.0943563953, green: 0.2551338077, blue: 0.2927393913, alpha: 1)
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
        return .lightContent
    }
    
    
}




//MARK:
//MARK: -----------Localisation Function

extension Login{
    
    func configureViewFromLocalisation() {
        if(UserDefaults.standard.value(forKey: "Contract_Language_EN") != nil){
            if(UserDefaults.standard.value(forKey: "Contract_Language_EN") as! Bool){
                if SetLanguage(arrayLanguages[1]) { }
                isEnglish = true
            }else{
                if SetLanguage(arrayLanguages[0]) { }
                isEnglish = false
            }
        }else{
            if SetLanguage(arrayLanguages[1]) { }
            isEnglish = true
        }

        
        if(isEnglish){
            img_Logo.image = UIImage(named: "logo1")
        }else{
            img_Logo.image = UIImage(named: "")
        }
        
        
        Textbox_email.placeholder = Localization("Contract_UserName")
        textbox_password.placeholder = Localization("Contract_Password")
        lbl_Login.text =  Localization("Contract_Login")
        lbl_RememberMe.text =  Localization("Contract_Rememberme")
        lbl_ForgotPass.text =  Localization("Contract_ForgotPass")
        login_bt.setTitle(Localization("Contract_Login"), for: .normal)
    }
}

extension Login : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == Textbox_email){
            var suggestionsArray = NSMutableArray()
            if(UserDefaults.standard.value(forKey: "SavedLogin") != nil){
                suggestionsArray = (UserDefaults.standard.value(forKey: "SavedLogin") as! NSArray).mutableCopy() as! NSMutableArray
                
            }
            return !autoCompleteText( in : textField, using: string, suggestionsArray: suggestionsArray as! [String])
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //textField.nextField?.becomeFirstResponder()
        if textField == self.Textbox_email {
            self.textbox_password.becomeFirstResponder()
            return false
        }else{
            self.view.endEditing(true)
        }
        return true
    }
    
    func autoCompleteText( in textField: UITextField, using string: String, suggestionsArray: [String]) -> Bool {
            if !string.isEmpty,
                let selectedTextRange = textField.selectedTextRange,
                selectedTextRange.end == textField.endOfDocument,
                let prefixRange = textField.textRange(from: textField.beginningOfDocument, to: selectedTextRange.start),
                let text = textField.text( in : prefixRange) {
                let prefix = text + string
                let matches = suggestionsArray.filter {
                    $0.hasPrefix(prefix)
                }
                if (matches.count > 0) {
                    textField.text = matches[0]
                    if let start = textField.position(from: textField.beginningOfDocument, offset: prefix.count) {
                        textField.selectedTextRange = textField.textRange(from: start, to: textField.endOfDocument)
                        return true
                    }
                }
            }
            return false
        }
}
