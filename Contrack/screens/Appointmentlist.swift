//
//  Appointmentlist.swift
//  Contrack
//
//  Created by Vikram Singh on 15/10/20.
//  Copyright © 2020 Vikram Singh. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher

class Appointmentlist: Common,APIDelegate,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tb_appointlist: UITableView!
    @IBOutlet weak var txt_filterstatus: UILabel!
    @IBOutlet weak var txt_filterdate: UILabel!
    @IBOutlet weak var bt_filter: UIButton!
    @IBOutlet weak var lbl_Title: UILabel!

    var appointarr = [Appointmodel]() , aryIncomplete =  [Appointmodel]() , aryInProgress =  [Appointmodel]() , ary_P_Complete =  [Appointmodel]() , aryComplete =  [Appointmodel]() , arytbl =   [Appointmodel]()
    public static var currentdate = "" , previousdate = "",propertyname = "",status = "incomplete",dateStatus = "Today"
    public static var checkforsearch = false
    
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()

    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        modalPresentationCapturesStatusBarAppearance = true
        self.tb_appointlist.delegate = self
        self.tb_appointlist.dataSource = self
        Appointmentlist.currentdate = getCurrentDate(format: "yyyy-MM-dd")
        Appointmentlist.previousdate = getCurrentDate(format: "yyyy-MM-dd")
        txt_filterdate.text = "\(Localization("Contract_Today")), "+getCurrentDate(format: "dd MMM yyyy")
       
     //   NotificationCenter.default.addObserver(self, selector: #selector(loaddata), name: NSNotification.Name(rawValue: "appointmentlist"), object: nil)
        
        let dict = NSMutableDictionary()
        dict.setValue(Appointmentlist.currentdate, forKey: "currentdate")
        dict.setValue(Appointmentlist.previousdate, forKey: "previousdate")
        dict.setValue(Appointmentlist.status, forKey: "status")
        dict.setValue(Appointmentlist.propertyname, forKey: "propertyname")
         dict.setValue(Appointmentlist.dateStatus, forKey: "datetype")

        UserDefaults.standard.setValue(dict, forKey: "FilterData")
        // Do any additional setup after loading the view.
   
    }

  
    override func viewWillAppear(_ animated: Bool) {
        self.view.endEditing(true)
        
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.first
            let topPadding = window?.safeAreaInsets.top
            let statusBar = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: topPadding ?? 0.0))
            statusBar.backgroundColor = Appointmentlist.hexStringToUIColor(hex: appThemeColor)
            UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.addSubview(statusBar)
        }
        
        configureViewFromLocalisation()
     
        if(checkLogoutSession()){
            let alert = UIAlertController(title: "Alert", message: "Session is expired , Please login again.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                UserDefaults.resetStandardUserDefaults()

                UserDefaults.standard.set(false, forKey: "checklogin")
                let generalVc = self.storyboard?.instantiateViewController(withIdentifier: "login") as! Login
                generalVc.modalPresentationStyle = .fullScreen
                self.present(generalVc, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)

        }else{
            loaddata()

        }
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
     //   self.locationManager.stopUpdatingLocation()
    }
  
    func checkLogoutSession()-> Bool {
        if UserDefaults.standard.value(forKey: "ContractApp_LogOutTime") != nil{
            let saveDate = UserDefaults.standard.value(forKey: "ContractApp_LogOutTime") as! Date
            if saveDate < Date() {
                
                return true
            }
        }
        else{
            let date = Date().addingTimeInterval(TimeInterval(15.0 * 60.0))

            UserDefaults.standard.setValue(date, forKey: "ContractApp_LogOutTime")
            return false

        }
        return false
    }
    func SettingView() {
        let alertController = UIAlertController (title: "Location Services Disabled!", message: "Please enable Location Based Services for better results! go to setting.", preferredStyle: .alert)

       let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in

           guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
               return
           }

           if UIApplication.shared.canOpenURL(settingsUrl) {
               UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                   print("Settings opened: \(success)") // Prints true
               })
           }
       }
       alertController.addAction(settingsAction)
     

       present(alertController, animated: true, completion: nil)
   }
    @objc func loaddata() {
        
        //print("current date = ",Appointmentlist.currentdate)
        //print("from date = ",Appointmentlist.previousdate)
        if(Appointmentlist.status == "all"){
            txt_filterstatus.text = "\(Localization("Contract_sts_All"))"
        }else if (Appointmentlist.status == "incomplete"){
            txt_filterstatus.text = "\(Localization("Contract_sts_Incomplete"))"
        }else if (Appointmentlist.status == "complete"){
            txt_filterstatus.text = "\(Localization("Contract_sts_Complete"))"
        }else if (Appointmentlist.status == "inprogress"){
            txt_filterstatus.text = "\(Localization("Contract_sts_InProgress"))"
        }
        else if (Appointmentlist.status == "partially_complete"){
            txt_filterstatus.text = "\(Localization("Contract_sts_PartiallyComplete"))"
        }
        if(Appointmentlist.dateStatus == "Today"){
            txt_filterdate.text = "\(Localization("Contract_Today")) - \(Appointmentlist.currentdate)"
        }else{
            txt_filterdate.text = "\(Appointmentlist.currentdate) \(Localization("Contract_To")) \(Appointmentlist.previousdate)"
        }

        appointarr.removeAll()
        let params = ["propertyName": Appointmentlist.propertyname, "fromDate": Appointmentlist.previousdate,"toDate": Appointmentlist.currentdate,"status": Appointmentlist.status]
        let url = "\(BASE_URL)appointment"
        postRequest(apiDelegate: self, requestCode: 1, url: url, params: params, isIndicatorShow: true)
    }
    
    
    @IBAction func logout_click(_ sender: Any) {
        let refreshAlert = UIAlertController(title: Localization("Contract_LogOut"), message: Localization("Contract_LogOutMessage"), preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: Localization("Contract_Cancel"), style: .default, handler: { (_: UIAlertAction!) in
            refreshAlert.dismiss(animated: true, completion: nil)
            
        }))
        refreshAlert.addAction(UIAlertAction(title: Localization("Contract_Ok"), style: .default, handler: { (_: UIAlertAction!) in
            refreshAlert.dismiss(animated: true, completion: nil)
            UserDefaults.standard.set(false, forKey: "checklogin")
            let generalVc = self.storyboard?.instantiateViewController(withIdentifier: "login") as! Login
            generalVc.modalPresentationStyle = .fullScreen
            self.present(generalVc, animated: true, completion: nil)
        }))
        self.present(refreshAlert, animated: true, completion: nil)
    }
    
    @IBAction func backClick(_ sender: Any) {
        if(Appointmentlist.checkforsearch){
            Appointmentlist.checkforsearch = false
            appointarr.removeAll()
            Appointmentlist.status = "incomplete"
            Appointmentlist.propertyname = ""
            Appointmentlist.currentdate = getCurrentDate(format: "yyyy-MM-dd")
            Appointmentlist.previousdate = getagodates(format: "yyyy-MM-dd",days:-30)
            txt_filterdate.text = "\(Localization("Contract_Today")), "+getCurrentDate(format: "dd MMM yyyy")
            loaddata()
        }
    }
    
    @IBAction func filterClikc(_ sender: Any) {
        let filterVc = self.storyboard?.instantiateViewController(withIdentifier: "filters") as! Filters
        filterVc.modalPresentationStyle = .fullScreen
        self.present(filterVc, animated: false, completion: nil)
    }
    
    
    @IBAction func actionOnLanguage(_ sender: Any) {
        let mainVc = storyboard?.instantiateViewController(withIdentifier: "LanguageSelectionVC") as! LanguageSelectionVC
        mainVc.modalPresentationStyle = .fullScreen
        mainVc.strComeFrom = "Update_Language"
        present(mainVc, animated: false, completion: nil)
        
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }

    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        let label = UILabel()
        let labelNorocord = UILabel()
        labelNorocord.text = ""
        var heightH = 40
        if(section == 0){
            label.text = "\(Localization("Contract_sts_InProgress"))  "
            if aryInProgress.count == 0 {
                labelNorocord.text = "\(Localization("Contract_NoRecord"))"
                heightH = 75
            }
        }else if(section == 1){
            label.text = "\(Localization("Contract_sts_Incomplete")) "
            if aryIncomplete.count == 0 {
                labelNorocord.text = "\(Localization("Contract_NoRecord"))"
                heightH = 75

            }
        }
        else if(section == 2){
            label.text = "\(Localization("Contract_sts_PartiallyComplete"))  "
            if ary_P_Complete.count == 0 {
                labelNorocord.text = "\(Localization("Contract_NoRecord"))"
                heightH = 75

            }
        }
        else if(section == 3){
            if aryComplete.count == 0 {
                labelNorocord.text = "\(Localization("Contract_NoRecord"))"
                heightH = 75

            }
            label.text = "\(Localization("Contract_sts_Complete"))  "
        }
        
        view.addSubview(label)
        view.backgroundColor = UIColor.systemGroupedBackground
        view.frame = CGRect(x: 0, y: 0, width: Int(tableView.frame.width), height: heightH)
     
        if heightH == 40 {
            label.frame = CGRect(x: 10, y: 0, width: Int(tableView.frame.width) - 20, height: heightH)

        }else{
            label.frame = CGRect(x: 10, y: 0, width: Int(tableView.frame.width) - 20, height: heightH / 2)
            labelNorocord.frame = CGRect(x: 0, y: label.frame.maxY, width: tableView.frame.width, height: CGFloat(heightH / 2))

            labelNorocord.backgroundColor = UIColor.white
            labelNorocord.font = UIFont.systemFont(ofSize: 12)
            labelNorocord.textAlignment = .center
            view.addSubview(labelNorocord)
        }
        
      

        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0){
            if aryInProgress.count == 0 {
                return 75.0
            }
        }else if(section == 1){
            if aryIncomplete.count == 0 {
                return 75.0            }
        }
        else if(section == 2){
            if ary_P_Complete.count == 0 {
                return 75.0
            }
        }
        else if(section == 3){
            if aryComplete.count == 0 {
                return 75.0
            }
        }
        
        return 40.0
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            return  aryInProgress.count
        }else if(section == 1){
            return  aryIncomplete.count
        }
        else if(section == 2){
            return  ary_P_Complete.count
        }
        else if(section == 3){
            return aryComplete.count
        }
        return 0
    }
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "appointcell", for: indexPath) as! Appointcell

        arytbl =   [Appointmodel]()
        if(indexPath.section == 0){
            arytbl = aryInProgress
        }else if(indexPath.section == 1){
            arytbl = aryIncomplete
        }
        else if(indexPath.section == 2){
            arytbl = ary_P_Complete
        }
        else if(indexPath.section == 3){
            arytbl = aryComplete
        }
        if(arytbl.count > 0){

            let url = URL(string: arytbl[indexPath.row].proimage)
            cell.img_appoint.kf.setImage(with: url)
            cell.txt_name.text = arytbl[indexPath.row].proname
            cell.txt_address.text = arytbl[indexPath.row].proaddress
            cell.txt_schedule.text = "\(Localization("Contract_Scheduled")) - "+arytbl[indexPath.row].workorderdate+", "+arytbl[indexPath.row].schedule
            cell.txt_technicial.text = "\(Localization("Contract_Technician")) - "+arytbl[indexPath.row].technician
            var appointstatus = ""
            let status = arytbl[indexPath.row].status
            if(status == "incomplete"){
                appointstatus = "\(Localization("Contract_sts_Incomplete"))"
            }else if(status == "inprogress"){
                 appointstatus = "\(Localization("Contract_sts_InProgress"))"
            }else if(status == "complete"){
                 appointstatus = "\(Localization("Contract_sts_Complete"))"
            }else if (status == "partially_complete"){
                appointstatus = "\(Localization("Contract_sts_PartiallyComplete"))"
            }
            cell.txt_status.text = "\(Localization("Contract_Status")) - "+appointstatus
            
        }
       
        return cell

        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selection = ",indexPath.row)
       

       var arytbltemp =   [Appointmodel]()
        if(indexPath.section == 0){
            arytbltemp = aryInProgress
        }else if(indexPath.section == 1){
            arytbltemp = aryIncomplete
        }
        else if(indexPath.section == 2){
            arytbltemp = ary_P_Complete
        }
        else if(indexPath.section == 3){
            arytbltemp = aryComplete
        }
        if(arytbltemp.count != 0){
            let generalVc = self.storyboard?.instantiateViewController(withIdentifier: "generalinfo") as! Generalinfo
            generalVc.modalPresentationStyle = .fullScreen
            generalVc.status = arytbltemp[indexPath.row].status
            generalVc.workorderid = arytbltemp[indexPath.row].workorderid
            self.present(generalVc, animated: true, completion: nil)
        }

        
    }
    func apiResponse(response: JSON, requestCode: Int) {
        if requestCode == 1 {
           // print(response)
            DispatchQueue.main.async {
                
                let dataarr = response.array! ?? nil
                if(dataarr!.count > 0){
                    var crewid = "", workorderid = "" , technician = "" , propertyname = "" ,
                    propertyimage = "" , address = "" , status = "" , sechedule = "" ,
                    workorderdate = "" , imagename = "" , imageid = ""
                    for i in dataarr!{
                        if let str = i["workorderId"].int {
                            workorderid = String(str)
                        }
                        if let str = i["crewId"].int {
                            crewid = String(str)
                        }
                        var firstname = "" , lastname = ""
                        if let str = i["crewFirstName"].string {
                            firstname = str
                        }
                        if let str = i["crewLastName"].string {
                            lastname = str
                        }
                        technician = firstname+" "+lastname
                        if let str = i["propertyAddress"].string {
                            address = str
                            
                        }
                        if let str = i["propertyName"].string {
                            propertyname = str
                        }
                        if let str = i["status"].string {
                            status = str
                        }
                        if let str = i["workOrderDate"].string {
                            workorderdate = str
                        }
                        if let str = i["scheduleTime"].string {
                            sechedule = str
                        }
                        let imagearr = i["propertyImage"].array!
                        for j in imagearr{
                            if let str = j["id"].int {
                                imageid = String(str)
                            }
                            if let str = j["imageData"].string {
                                propertyimage = str
                            }
                            if let str = j["name"].string {
                                imagename = String(str)
                            }
                        }
                        
                        let temp = Appointmodel(proimage: propertyimage, proaddress: address,schedule: sechedule,technician: technician,status: status
                            ,crewid: crewid,workorderid: workorderid,workorderdate: workorderdate,imagename:imagename,imageid:imageid,proname: propertyname)
                        
                        
                        
                        
                        
                        self.appointarr.append(temp)
                        
                    }
                    
                }
                
                self.FilterByStatus()
              //  self.tb_appointlist.reloadData()
                
            }
        }
        
        
    }
    
    func FilterByStatus() {
    
        aryIncomplete.removeAll()
        aryInProgress.removeAll()
        ary_P_Complete.removeAll()
        aryComplete.removeAll()


        for item in appointarr {
            let status = "\(((item as AnyObject) as! Appointmodel).status)"
       
            if(status == "\(enum_Status.incomplete)"){
                aryIncomplete.append(item)
            }else if(status == "\(enum_Status.inprogress)"){
                aryInProgress.append(item)

            }else if(status == "\(enum_Status.complete)"){
                aryComplete.append(item)

            }else if (status == "\(enum_Status.partially_complete)"){
                ary_P_Complete.append(item)
            }
        }
     
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"

        for item in aryIncomplete {
            print(item.schedule)
            print(item.workorderdate)

        }
        
        self.aryIncomplete = self.aryIncomplete.sorted(by: {
            df.date(from:($0 ).workorderdate + " " +  ($0 ).schedule)! <= df.date(from:($1).workorderdate + " " +  ($1).schedule)!
        })
        self.aryInProgress = self.aryInProgress.sorted(by: {
            df.date(from:($0 ).workorderdate + " " +  ($0 ).schedule)! <= df.date(from:($1).workorderdate + " " +  ($1).schedule)!
        })
        self.aryComplete = self.aryComplete.sorted(by: {
            df.date(from:($0 ).workorderdate + " " +  ($0 ).schedule)! <= df.date(from:($1).workorderdate + " " +  ($1).schedule)!
        })
        self.ary_P_Complete = self.ary_P_Complete.sorted(by: {
            df.date(from:($0 ).workorderdate + " " +  ($0 ).schedule)! <= df.date(from:($1).workorderdate + " " +  ($1).schedule)!
        })
        self.tb_appointlist.reloadData()
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor =  #colorLiteral(red: 0.0943563953, green: 0.2551338077, blue: 0.2927393913, alpha: 1)
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
        return .lightContent
    }
    
    
    
    
}


//MARK:
//MARK: -----------Localisation Function

extension Appointmentlist{
    
    func configureViewFromLocalisation() {
        
        if(UserDefaults.standard.value(forKey: "Contract_Language_EN") != nil){
            if(UserDefaults.standard.value(forKey: "Contract_Language_EN") as! Bool){
                if SetLanguage(arrayLanguages[1]) { }
                isEnglish = true
            }else{
                if SetLanguage(arrayLanguages[0]) { }
                isEnglish = false
            }
        }else{
            if SetLanguage(arrayLanguages[1]) { }
            isEnglish = true
        }

     lbl_Title.text =  Localization("Contract_AppointmentList")

    }
}





enum enum_Status: String {
    
    case incomplete = "incomplete"
    case inprogress = "inprogress"
    case complete = "complete"
    case partially_complete = "partially_complete"
    
}
func utcToLocal(dateStr: String) -> String? {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "H:mm:ss"
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    
    if let date = dateFormatter.date(from: dateStr) {
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "h:mm a"
    
        return dateFormatter.string(from: date)
    }
    return nil
}
