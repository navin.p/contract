//
//  Splash.swift
//  Contrack
//
//  Created by Vikram Singh on 14/10/20.
//  Copyright © 2020 Vikram Singh. All rights reserved.
//

import UIKit

class Splash: UIViewController {
    var timer = Timer()
    override func viewDidLoad() {
       
        super.viewDidLoad()
        modalPresentationCapturesStatusBarAppearance = true
        runTimer()
        
        NotificationCenter.default.addObserver(self, selector: #selector(dismisssplash), name: NSNotification.Name(rawValue: "dismisssplash"), object: nil)
    }
    
    @objc func dismisssplash() {
        dismiss(animated: true, completion: nil)
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: false)
    }
    
    @objc func updateTimer() {

        if(UserDefaults.standard.value(forKey: "Contract_Language_EN") != nil){
            viewRedirection()
        }else{
         //   viewRedirection()

            let mainVc = storyboard?.instantiateViewController(withIdentifier: "LanguageSelectionVC") as! LanguageSelectionVC
            mainVc.modalPresentationStyle = .fullScreen
            present(mainVc, animated: true, completion: nil)
        }
        
        
   
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor =  #colorLiteral(red: 0.0943563953, green: 0.2551338077, blue: 0.2927393913, alpha: 1)
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
        return .lightContent
    }
    
    func viewRedirection() {
        let checklogin = UserDefaults.standard.bool(forKey: "checklogin")
        if checklogin {
            let mainVc = storyboard?.instantiateViewController(withIdentifier: "appointmentlist") as! Appointmentlist
            mainVc.modalPresentationStyle = .fullScreen
            present(mainVc, animated: true, completion: nil)
        } else {
            let loginvc = storyboard?.instantiateViewController(withIdentifier: "login") as! Login
            loginvc.modalPresentationStyle = .fullScreen
            present(loginvc, animated: true, completion: nil)
        }
    }
    
}



