//
//  Inprogresstasklist.swift
//  Contrack
//
//  Created by Vikram Singh on 07/11/20.
//  Copyright © 2020 Vikram Singh. All rights reserved.
//

import SwiftUI
import DLRadioButton

class Inprogresstasklist: UIView {

    @IBOutlet weak var Bt_inprogress: UIButton!
    @IBOutlet weak var stack_holder: UIStackView!
    @IBOutlet weak var Chkbox_task: DLRadioButton!
    
    @IBOutlet weak var Lb_checkbox: UILabel!
}
