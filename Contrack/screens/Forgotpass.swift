//
//  Forgotpass.swift
//  Contrack
//
//  Created by Vikram Singh on 14/10/20.
//  Copyright © 2020 Vikram Singh. All rights reserved.
//

import UIKit
import SwiftyJSON
class Forgotpass: Common, APIDelegate,UITextFieldDelegate{
    @IBOutlet weak var textbox_email: UITextField!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContain: UILabel!
    @IBOutlet weak var btnDone: UIButton!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textbox_email.delegate = self
        configureViewFromLocalisation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.first
            let topPadding = window?.safeAreaInsets.top
            let statusBar = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: topPadding ?? 0.0))
            statusBar.backgroundColor = Forgotpass.hexStringToUIColor(hex: appThemeColor)
            UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.addSubview(statusBar)

        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           self.textbox_email.becomeFirstResponder()
            self.view.endEditing(true)
           return true
       }
    
    
    @IBAction func doneclick(_ sender: Any) {
        let emailvalue = textbox_email.text!
        if(emailvalue.isEmpty || isValidEmail(testStr: emailvalue) == false){
            showWarningToast(title: Localization("Contract_FieldRequired"), message: Localization("Contract_ValidEmailAddress"))
        }else{
            let params = ["email": emailvalue]
            let url = "\(BASE_URL)forgot-password"
            loginpost(apiDelegate: self, requestCode: 1, url: url, params: params, isIndicatorShow: true)
        }
    }
    
    @IBAction func backclick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor =  #colorLiteral(red: 0.0943563953, green: 0.2551338077, blue: 0.2927393913, alpha: 1)
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
        return .lightContent
    }
    
    func apiResponse(response: JSON, requestCode: Int) {
        if requestCode == 1 {
            print(response)
            DispatchQueue.main.async {
                let message = response["message"].string
                let refreshAlert = UIAlertController(title: "Success", message: message, preferredStyle: UIAlertController.Style.alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_: UIAlertAction!) in
                    self.dismiss(animated: true, completion: nil)
                }))
                DispatchQueue.main.async {
                    self.present(refreshAlert, animated: true, completion: nil)
                }
                
            }
        }
       
        
    }
    
}


//MARK:
//MARK: -----------Localisation Function

extension Forgotpass{
    func configureViewFromLocalisation() {
        
        textbox_email.placeholder = Localization("Contract_EmailAddress")
        lblTitle.text =  Localization("Contract_ForgotPass")
        lblContain.text =  Localization("Contract_ForgotPassMessage")
        btnDone.setTitle(Localization("Contract_Done"), for: .normal)
    }
}
