//
//  Filters.swift
//  Contrack
//
//  Created by Vikram Singh on 03/11/20.
//  Copyright © 2020 Vikram Singh. All rights reserved.
//

import UIKit
import DLRadioButton
class Filters: Common,UITextFieldDelegate{
    
    
    @IBOutlet weak var Lb_title: UILabel!
    @IBOutlet weak var btn_Apply: UIButton!
    @IBOutlet weak var btn_Clear: UIButton!
    @IBOutlet weak var Lb_Property: UILabel!
    @IBOutlet weak var Lb_Day: UILabel!
    @IBOutlet weak var Lb_Day_Yesterday: UILabel!
    @IBOutlet weak var Lb_Day_Today: UILabel!
    @IBOutlet weak var Lb_Day_ThisWeek: UILabel!
    @IBOutlet weak var Lb_Day_LastWeek: UILabel!
    @IBOutlet weak var Lb_Custome: UILabel!

    @IBOutlet weak var Lb_Date: UILabel!
    @IBOutlet weak var Lb_To: UILabel!

    @IBOutlet weak var Lb_Status: UILabel!
    @IBOutlet weak var Lb_All: UILabel!
    @IBOutlet weak var Lb_Incomplete: UILabel!
    @IBOutlet weak var Lb_Complete: UILabel!
    @IBOutlet weak var Lb_InProgress: UILabel!
    @IBOutlet weak var Lb_PartiallyComplete: UILabel!

    @IBOutlet weak var Rb_lastweek: DLRadioButton!
    @IBOutlet weak var Lb_todate: UITextField!
    @IBOutlet weak var Lb_fromdate: UITextField!
    
    @IBOutlet weak var Tf_propertyname: UITextField!
    @IBOutlet weak var Rb_partially: DLRadioButton!
    @IBOutlet weak var Rb_thisweek: DLRadioButton!
    @IBOutlet weak var Rb_today: DLRadioButton!
    @IBOutlet weak var Rb_yesterday: DLRadioButton!
    @IBOutlet weak var Rb_Custome: DLRadioButton!

    @IBOutlet weak var Rb_all: DLRadioButton!
    
    @IBOutlet weak var Rb_incomplete: DLRadioButton!
    
    @IBOutlet weak var Rb_complete: DLRadioButton!
    
    
    @IBOutlet weak var Rb_inprogress: DLRadioButton!
    
    @IBOutlet weak var height_CustomeDate: NSLayoutConstraint!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewFromLocalisation()
        Tf_propertyname.delegate = self
        Rb_yesterday.tag = 0
        Rb_today.tag = 1
        Rb_thisweek.tag = 2
        Rb_lastweek.tag = 3
        Rb_Custome.tag = 9

        Rb_all.tag = 4
        Rb_incomplete.tag = 5
        Rb_complete.tag = 6
        Rb_inprogress.tag = 7
        Rb_partially.tag = 8
        height_CustomeDate.constant = 0.0
        
        
        
     
        if(UserDefaults.standard.value(forKey: "FilterData") != nil){
            let dict = UserDefaults.standard.value(forKey: "FilterData")as! NSDictionary
            Tf_propertyname.text = "\(dict.value(forKey: "propertyname")!)"
            if dict.value(forKey: "datetype") as! String == "Today" {
                Rb_yesterday.isSelected = false
                Rb_today.isSelected = true
                Rb_thisweek.isSelected = false
                Rb_lastweek.isSelected = false
                Rb_Custome.isSelected = false

                Lb_fromdate.text = getCurrentDate(format: "yyyy-MM-dd")
                Lb_todate.text = getCurrentDate(format: "yyyy-MM-dd")
                Appointmentlist.dateStatus = "Today"

            }else  if dict.value(forKey: "datetype") as! String == "yesterday" {
                Appointmentlist.dateStatus = "yesterday"

                Rb_yesterday.isSelected = true
                Rb_today.isSelected = false
                Rb_thisweek.isSelected = false
                Rb_lastweek.isSelected = false
                Rb_Custome.isSelected = false

                Lb_fromdate.text = getagodates(format: "yyyy-MM-dd", days: -1)
                Lb_todate.text = getagodates(format: "yyyy-MM-dd", days: -1)
            }else  if dict.value(forKey: "datetype") as! String == "ThisWeek" {
                Rb_yesterday.isSelected = false
                Rb_today.isSelected = false
                Rb_thisweek.isSelected = true
                Rb_lastweek.isSelected = false
                Rb_Custome.isSelected = false

                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                let firstdate = formatter.string(from: getweekstartdate(days: 1))
                let lastdate = formatter.string(from: getweekstartdate(days: 7))
                print("start date = ",firstdate)
                print("last date = ",lastdate)
                Lb_fromdate.text = firstdate
                Lb_todate.text = lastdate
                Appointmentlist.dateStatus = "ThisWeek"
            }else  if dict.value(forKey: "datetype") as! String == "LastWeek" {
                Rb_yesterday.isSelected = false
                Rb_today.isSelected = false
                Rb_thisweek.isSelected = false
                Rb_lastweek.isSelected = true
                Rb_Custome.isSelected = false

                Appointmentlist.dateStatus = "LastWeek"

                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                let firstdate = formatter.string(from: getweekstartdate(days: -6))
                //var firstdate = lastweekfirstdate()
                let lastdate = formatter.string(from: getweekstartdate(days: 0))
                print("start date = ",firstdate)
                print("last date = ",lastdate)
                Lb_fromdate.text = firstdate
                Lb_todate.text = lastdate
                
            }else {
                Rb_yesterday.isSelected = false
                Rb_today.isSelected = false
                Rb_thisweek.isSelected = false
                Rb_lastweek.isSelected = false
                Rb_Custome.isSelected = true
                height_CustomeDate.constant = 65.0
                Lb_todate.text = "\(dict.value(forKey: "currentdate") ?? "")"
                Lb_fromdate.text = "\(dict.value(forKey: "previousdate") ?? "")"
            }
            
            
            //------Status
          
            if dict.value(forKey: "status") as! String == "all" {
                Rb_all.isSelected = true
                Rb_incomplete.isSelected = false
                Rb_complete.isSelected = false
                Rb_inprogress.isSelected = false
                Rb_partially.isSelected = false
                Appointmentlist.status = "all"
            }
            else if dict.value(forKey: "status") as! String == "incomplete" {
                Rb_all.isSelected = false
                Rb_incomplete.isSelected = true
                Rb_complete.isSelected = false
                Rb_inprogress.isSelected = false
                Rb_partially.isSelected = false
                Appointmentlist.status = "incomplete"
            } else if dict.value(forKey: "status") as! String == "complete" {
                Rb_all.isSelected = false
                Rb_incomplete.isSelected = false
                Rb_complete.isSelected = true
                Rb_inprogress.isSelected = false
                Rb_partially.isSelected = false
                Appointmentlist.status = "complete"
            }
            else if dict.value(forKey: "status") as! String == "inprogress" {
                Rb_all.isSelected = false
                Rb_incomplete.isSelected = false
                Rb_complete.isSelected = false
                Rb_inprogress.isSelected = true
                Rb_partially.isSelected = false
                Appointmentlist.status = "inprogress"
            }    else if dict.value(forKey: "status") as! String == "partially_complete" {
                Rb_all.isSelected = false
                Rb_incomplete.isSelected = false
                Rb_complete.isSelected = false
                Rb_inprogress.isSelected = false
                Rb_partially.isSelected = true
                Appointmentlist.status = "partially_complete"
            }
            
        }
        else{
            Rb_today.isSelected = true
            Rb_incomplete.isSelected = true
            Lb_fromdate.text = getCurrentDate(format: "yyyy-MM-dd")
            Lb_todate.text = getCurrentDate(format: "yyyy-MM-dd")
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.first
            let topPadding = window?.safeAreaInsets.top
            let statusBar = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: topPadding ?? 0.0))
            statusBar.backgroundColor = Filters.hexStringToUIColor(hex: appThemeColor)
            UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.addSubview(statusBar)
        }
    }
    
    
    @IBAction func yesterdayClick(_ sender: Any) {
        Appointmentlist.dateStatus = "yesterday"

        Rb_yesterday.isSelected = true
        Rb_today.isSelected = false
        Rb_thisweek.isSelected = false
        Rb_lastweek.isSelected = false
        Rb_Custome.isSelected = false

        Lb_fromdate.text = getagodates(format: "yyyy-MM-dd", days: -1)
        Lb_todate.text = getagodates(format: "yyyy-MM-dd", days: -1)
        height_CustomeDate.constant = 0.0
    }
    
    @IBAction func todayClick(_ sender: Any) {
        Appointmentlist.dateStatus = "Today"

        Rb_yesterday.isSelected = false
        Rb_today.isSelected = true
        Rb_thisweek.isSelected = false
        Rb_lastweek.isSelected = false
        Rb_Custome.isSelected = false

        Lb_fromdate.text = getCurrentDate(format: "yyyy-MM-dd")
        Lb_todate.text = getCurrentDate(format: "yyyy-MM-dd")
        height_CustomeDate.constant = 0.0

    }
    
    @IBAction func thisweekClick(_ sender: Any) {
        Rb_yesterday.isSelected = false
        Rb_today.isSelected = false
        Rb_thisweek.isSelected = true
        Rb_lastweek.isSelected = false
        Rb_Custome.isSelected = false

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let firstdate = formatter.string(from: getweekstartdate(days: 1))
        let lastdate = formatter.string(from: getweekstartdate(days: 7))
        print("start date = ",firstdate)
        print("last date = ",lastdate)
        Lb_fromdate.text = firstdate
        Lb_todate.text = lastdate
        Appointmentlist.dateStatus = "ThisWeek"
        height_CustomeDate.constant = 0.0

    }
    
    @IBAction func lastweekClick(_ sender: Any) {
        Rb_yesterday.isSelected = false
        Rb_today.isSelected = false
        Rb_thisweek.isSelected = false
        Rb_lastweek.isSelected = true
        Rb_Custome.isSelected = false

        Appointmentlist.dateStatus = "LastWeek"

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let firstdate = formatter.string(from: getweekstartdate(days: -6))
        //var firstdate = lastweekfirstdate()
        let lastdate = formatter.string(from: getweekstartdate(days: 0))
        print("start date = ",firstdate)
        print("last date = ",lastdate)
        Lb_fromdate.text = firstdate
        Lb_todate.text = lastdate
        
        height_CustomeDate.constant = 0.0

    }
    @IBAction func CustomeClick(_ sender: Any) {
        Rb_yesterday.isSelected = false
        Rb_today.isSelected = false
        Rb_thisweek.isSelected = false
        Rb_lastweek.isSelected = false
        Rb_Custome.isSelected = true
        Appointmentlist.dateStatus = "Custome"

       
        Lb_fromdate.text = ""
        Lb_todate.text = ""
        height_CustomeDate.constant = 65.0

        
    }
    
    @IBAction func allclick(_ sender: Any) {
        Rb_all.isSelected = true
        Rb_incomplete.isSelected = false
        Rb_complete.isSelected = false
        Rb_inprogress.isSelected = false
        Rb_partially.isSelected = false
        Appointmentlist.status = "all"
    }
    
    @IBAction func incompleteClick(_ sender: Any) {
        Rb_all.isSelected = false
        Rb_incomplete.isSelected = true
        Rb_complete.isSelected = false
        Rb_inprogress.isSelected = false
        Rb_partially.isSelected = false
        Appointmentlist.status = "incomplete"
    }
    
    @IBAction func completeClick(_ sender: Any) {
        Rb_all.isSelected = false
        Rb_incomplete.isSelected = false
        Rb_complete.isSelected = true
        Rb_inprogress.isSelected = false
        Rb_partially.isSelected = false
        Appointmentlist.status = "complete"
    }
    
    
    @IBAction func backClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func inprogressClick(_ sender: Any) {
        Rb_all.isSelected = false
        Rb_incomplete.isSelected = false
        Rb_complete.isSelected = false
        Rb_inprogress.isSelected = true
        Rb_partially.isSelected = false
        Appointmentlist.status = "inprogress"
    }
    
    @IBAction func partiallyClick(_ sender: Any) {
        Rb_all.isSelected = false
        Rb_incomplete.isSelected = false
        Rb_complete.isSelected = false
        Rb_inprogress.isSelected = false
        Rb_partially.isSelected = true
        Appointmentlist.status = "partially_complete"
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.Tf_propertyname.becomeFirstResponder()
        self.view.endEditing(true)
        return true
    }
    
    @IBAction func ClearFilter(_ sender: Any) {
        Appointmentlist.propertyname = ""
        Appointmentlist.currentdate = getCurrentDate(format: "yyyy-MM-dd")
        Appointmentlist.previousdate = getCurrentDate(format: "yyyy-MM-dd")
        Appointmentlist.status = "incomplete"
        Appointmentlist.dateStatus = "Today"

        let dict = NSMutableDictionary()
        dict.setValue(Appointmentlist.currentdate, forKey: "currentdate")
        dict.setValue(Appointmentlist.previousdate, forKey: "previousdate")
        dict.setValue(Appointmentlist.status, forKey: "status")
        dict.setValue(Appointmentlist.propertyname, forKey: "propertyname")
        dict.setValue(Appointmentlist.dateStatus, forKey: "datetype")
        
        UserDefaults.standard.setValue(dict, forKey: "FilterData")
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "appointmentlist"), object: nil)
        dismiss(animated: true, completion: nil)
        
        
    }
    @IBAction func applyClick(_ sender: UIButton) {
        Appointmentlist.propertyname = Tf_propertyname.text!
        Appointmentlist.currentdate = Lb_todate.text!
        Appointmentlist.previousdate = Lb_fromdate.text!
        Appointmentlist.checkforsearch = true
        
        let dict = NSMutableDictionary()
        dict.setValue(Appointmentlist.currentdate, forKey: "currentdate")
        dict.setValue(Appointmentlist.previousdate, forKey: "previousdate")
        dict.setValue(Appointmentlist.status, forKey: "status")
        dict.setValue(Appointmentlist.propertyname, forKey: "propertyname")
        dict.setValue(Appointmentlist.dateStatus, forKey: "datetype")

        
        UserDefaults.standard.setValue(dict, forKey: "FilterData")
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "appointmentlist"), object: nil)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func fromdateClick(_ sender: Any) {
        datePickerTapped_TextFiled(textField: Lb_fromdate,format: "yyyy-MM-dd",check: "")
    }
    
    @IBAction func todateClick(_ sender: Any) {
        datePickerTapped_TextFiled(textField: Lb_todate,format: "yyyy-MM-dd",check: "")
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor =  #colorLiteral(red: 0.0943563953, green: 0.2551338077, blue: 0.2927393913, alpha: 1)
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
        return .lightContent
    }
    
    
    
}

//MARK:
//MARK: -----------Localisation Function

extension Filters{
    
    func configureViewFromLocalisation() {
        
        Lb_title.text =  Localization("Contract_Filters")
        btn_Apply .setTitle(Localization("Contract_Apply"), for: .normal)
        btn_Clear .setTitle(Localization("Contract_Clear"), for: .normal)

        Lb_Property.text =  Localization("Contract_Property")

        Lb_Day.text =  Localization("Contract_Days")
        Lb_Day_Yesterday.text =  Localization("Contract_Yesterday")
        Lb_Day_Today.text =  Localization("Contract_Today")
        Lb_Day_ThisWeek.text =  Localization("Contract_ThisWeek")
        Lb_Day_LastWeek.text =  Localization("Contract_LastWeek")
        Lb_Custome.text =  Localization("Contract_Custome")

        Lb_Date.text =  Localization("Contract_Date")
        Lb_To.text =  Localization("Contract_To")

        Lb_Status.text =  Localization("Contract_Status")
        Lb_All.text =  Localization("Contract_sts_All")
        Lb_Incomplete.text =  Localization("Contract_sts_Incomplete")
        Lb_Complete.text =  Localization("Contract_sts_Complete")
        Lb_InProgress.text =  Localization("Contract_sts_InProgress")
        Lb_PartiallyComplete.text =  Localization("Contract_sts_PartiallyComplete")

        Lb_fromdate.placeholder = Localization("Contract_StartDate")
        Lb_todate.placeholder = Localization("Contract_Enddate")



        

    }
}
